defmodule Database do
    use GenServer

    #Client
    def start_link(arg) do
        IO.inspect arg
        IO.puts "Hello"
        GenServer.start_link(__MODULE__, arg, name: __MODULE__)
    end

    def get(pid) do
        GenServer.call(pid, :get)
    end

    def push(pid, value) do
        GenServer.cast(pid, {:put, value})
    end

    def delete(pid, value) do
        GenServer.cast(pid, {:delete, value})
    end

    #Server (CallBacks)
    @impl true
    def init(list) do
        IO.puts "Holaaaa guyyy"
        {:ok, list}
    end

    @impl true
    def handle_cast({:put, value}, intern_state) do
        updated_state = intern_state ++ [value]
        {:noreply, updated_state}
    end

    @impl true
    def handle_cast({:delete, value}, intern_state) do
        updated_state = intern_state -- [value]
        {:noreply, updated_state}
    end

    @impl true
    def handle_call(:get, _from, intern_state) do
        {:reply, intern_state, intern_state}
    end

end
