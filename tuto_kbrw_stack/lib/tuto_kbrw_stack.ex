defmodule TutoKbrwStack do
  def start(_type, _args) do

    children = [{Database, [1, 2, 3]}]
    opts = [strategy: :one_for_one]
    Supervisor.start_link(children, opts)
  end
  @moduledoc """
  Documentation for `TutoKbrwStack`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TutoKbrwStack.hello()
      :world

  """
  def hello do
    :world
  end
end
