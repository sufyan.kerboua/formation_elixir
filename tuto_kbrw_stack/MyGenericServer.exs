# import Kernel

defmodule AccountServer do
  def handle_cast({:credit, c}, amount), do: amount + c
  def handle_cast({:debit, c}, amount), do: amount - c
  def handle_call(:get, amount) do
    #Return the response of the call, and the new inner state of the server
    amount
  end

  def start_link(initial_amount) do
    MyGenericServer.start_link(AccountServer,initial_amount)
  end
end

defmodule MyGenericServer do
    # Main process
    def loop({callback_module, server_state}) do
      receive do
        {:get, sender} -> 
          current_amount = callback_module.handle_call(:get, server_state)
          Kernel.send(sender, current_amount)
          loop({callback_module, server_state})
        {req, sender} -> 
          amount = callback_module.handle_cast(req, server_state)
          Kernel.send(sender, amount)
          loop({callback_module, amount})
      end
    end

    def cast(process_pid, request) do
      Kernel.send(process_pid, {request, self()})
      receive do amount-> amount end
    end

    def call(process_pid, request) do
      Kernel.send(process_pid, {request, self()})
      receive do amount-> amount end
    end

    def start_link(callack_module, server_initial_state) do
      your_process_pid = spawn_link(fn->
        loop({callack_module, server_initial_state})
      end)
      {:ok, your_process_pid}
    end
end

{:ok, my_account} = AccountServer.start_link(4)
MyGenericServer.cast(my_account, {:credit, 5})
MyGenericServer.cast(my_account, {:credit, 2})
MyGenericServer.cast(my_account, {:debit, 3})
amount = MyGenericServer.call(my_account, :get)
IO.puts "current credit hold is #{amount}"
