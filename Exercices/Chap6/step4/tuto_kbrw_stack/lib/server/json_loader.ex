defmodule JsonLoader do

  def load_to_database(database, json_file) do
    with {:ok, body} <- File.read(json_file),
        {:ok, json} <- Poison.decode(body) do

          stream = Task.async_stream(json, fn
            element ->
              Riak.put_json_object_to_bucket Poison.encode!(element), element["id"]
          end,
          [max_concurrency: 10])

        end
  end

end
# JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
