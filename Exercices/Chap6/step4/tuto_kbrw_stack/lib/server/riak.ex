defmodule Riak do
  @schema_name "sufyan_orders_schema"
  @index_name "sufyan_orders_index"
  @bucket_name "sufyan_orders"

  def url, do: "https://kbrw-sb-tutoex-riak-gateway.kbrw.fr"

  def auth_header do
    username = "sophomore"
    password = "jlessthan3tutoex"
    auth = :base64.encode_to_string("#{username}:#{password}")
    [{'authorization', 'Basic #{auth}'}]
  end

  defp read_file(filepath) do
    with {:ok, raw_data} <- File.read(filepath) do
          IO.inspect raw_data
      end
  end

  def upload_schema(path_to_schema) do
    data = read_file path_to_schema
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:put, {'#{Riak.url}/search/schema/#{@schema_name}', Riak.auth_header(), 'application/xml', data}, [], [])
  end

  def create_index do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:put, {'#{Riak.url}/search/index/#{@index_name}', Riak.auth_header(), 'application/json', '{"schema": "#{@schema_name}"}'}, [], [])
  end

  def assign_index do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:put, {'#{Riak.url}/buckets/#{@bucket_name}/props', Riak.auth_header(), 'application/json', '{"props":{"search_index": "#{@index_name}"}}'}, [], [])
  end

  def fetch_indexes do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/search/index', Riak.auth_header()}, [], [])
    body
  end

  def clear_bucket do
    payload = Riak.get_keys_of_buckets |> Poison.decode!
    Enum.map(payload["keys"], fn key -> delete_object_from_key key end)
    :ok
  end

  def remove_bucket do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:delete, {'#{Riak.url}/buckets/#{@bucket_name}/props', Riak.auth_header()}, [], [])
  end

  # TMP

  def get_bucket_prop do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{@bucket_name}/props', Riak.auth_header()}, [], [])
    body
  end

  def get_list_buckets do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets?buckets=true', Riak.auth_header()}, [], [])
    body
  end

  def put_object_in_bucket do
    data = "salut"
    {:ok, {{_, 201, _message}, _headers, body}} = :httpc.request(:post, {'#{Riak.url}/buckets/#{@bucket_name}/keys', Riak.auth_header(), 'text/plain', data}, [], [])
    body
  end

  def put_json_object_to_bucket(json_payload, key) do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:post, {'#{Riak.url}/buckets/#{@bucket_name}/keys/#{key}', Riak.auth_header(), 'application/json', json_payload}, [], [])
  end

  def get_keys_of_buckets do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{@bucket_name}/keys?keys=true', Riak.auth_header()}, [], [])
    body
  end

  def get_object_from_key(key) do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{@bucket_name}/keys/#{key}', Riak.auth_header()}, [], [])
    IO.inspect body, char_lists: :as_charlists
  end

  def delete_object_from_key(key) do
    {:ok, {{_, 204, _message}, _headers, body}} = :httpc.request(:delete, {'#{Riak.url}/buckets/#{@bucket_name}/keys/#{key}', Riak.auth_header()}, [], [])
    body
  end

end
