defmodule Server.Router do
  import Server.TheCreator
  use Server.TheCreator
  use Plug.Router
  import Plug.Conn.Query
  import Plug.Static

  plug Plug.Static, from: "priv/static", at: "/static"
  plug(:match)
  plug(:dispatch)

  # API Endpoints
  get "/api/orders/" do
    queries = String.split(conn.query_string, "&") |> Enum.map(fn query -> String.split(query, "=") end)
    page = queries |> Enum.filter(fn payload -> List.first(payload) == "page" end) |> List.first
    rows = queries |> Enum.filter(fn payload -> List.first(payload) == "rows" end) |> List.first
    sort = queries |> Enum.filter(fn payload -> List.first(payload) == "sort" end) |> List.first
    query = queries |> Enum.filter(fn payload -> List.first(payload) !== "page" and List.first(payload) !== "rows" end) |> Enum.map(fn payload -> Enum.join(payload, ":") end) |>  Enum.join("%20AND%20")
    # raw_bdd = Server.Database.get(Server.Database)
    raw_bdd = Server.Database.search(Server.Database, {query, page, rows, sort})
    formated_res = Poison.encode!(raw_bdd)
    send_resp(conn, 200, formated_res)
  end

  get "/api/order/:order_id" do
    # raw_bdd = Server.Database.get(Server.Database, order_id) |> List.first |> Tuple.to_list |> List.last
    raw_bdd = Server.Database.get(Server.Database, order_id)
    IO.inspect raw_bdd
    test = Poison.encode!(raw_bdd)
    send_resp(conn, 200, test)
  end

  delete "/api/order/:order_id" do
    raw_res = Server.Database.delete(Server.Database, order_id)
    res = Poison.encode!(raw_res)
    :timer.sleep 2000
    send_resp(conn, 200, res)
  end

  get "/search/" do
    criteria = String.split(conn.query_string, "&") |>
    Enum.reduce([],
    fn
      query, acc ->
        {key, value} = List.first(Map.to_list(decode(query)))
        payload = if Integer.parse(value) !== :error, do: {key, String.to_integer(value)}, else: {key, value}
        acc ++ [payload]
    end)

    data = Server.Database.search(Server.Database, criteria) |> Macro.to_string()
    send_resp(conn, 200, data)
  end

  # post "/" do
  #   {:ok, body, _conn} = Plug.Conn.read_body(conn)
  #   data_to_push = Poison.decode!(body)
  #   payload = %{id: data_to_push["id"], key: data_to_push["key"]}
  #   Server.Database.push(Server.Database, payload)
  #   send_resp(conn, 201, "Element added")
  # end

  delete "/delete" do
    {:ok, body, _conn} = Plug.Conn.read_body(conn)
    data_to_delete = Poison.decode!(body)
    payload = data_to_delete["key"]
    Server.Database.delete(Server.Database, payload)
    send_resp(conn, 405, "Element deleted")
  end

  # Client route
  get _, do: send_file(conn, 200, "priv/static/index.html")

  match _, do: send_resp(conn, 404, "Page Not Found")

end
