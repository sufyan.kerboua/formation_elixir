defmodule Riak do
  @schema_name "sufyan_orders_schema"
  @index_name "sufyan_orders_index"
  @bucket_name "sufyan_orders"

  def url, do: "https://kbrw-sb-tutoex-riak-gateway.kbrw.fr"

  def auth_header do
    username = "sophomore"
    password = "jlessthan3tutoex"
    auth = :base64.encode_to_string("#{username}:#{password}")
    [{'authorization', 'Basic #{auth}'}]
  end

  def read_file(filepath) do
    with {:ok, raw_data} <- File.read(filepath) do
          IO.inspect raw_data
      end
  end

  def upload_schema(path_to_schema, schema_name \\ @schema_name) do
    data = Riak.read_file(path_to_schema)
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:put, {'#{Riak.url}/search/schema/#{schema_name}', Riak.auth_header(), 'application/xml', data}, [], [])
  end

  def create_index(index_name \\ @index_name, schema_name \\ @schema_name) do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:put, {'#{Riak.url}/search/index/#{index_name}', Riak.auth_header(), 'application/json', '{"schema": "#{schema_name}"}'}, [], [])
  end

  def assign_index(bucket_name \\ @bucket_name, index_name \\ @index_name) do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:put, {'#{Riak.url}/buckets/#{bucket_name}/props', Riak.auth_header(), 'application/json', '{"props":{"search_index": "#{index_name}"}}'}, [], [])
  end

  def fetch_indexes do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/search/index', Riak.auth_header()}, [], [])
    body
  end

  def clear_bucket(bucket_name \\ @bucket_name) do
    payload = get_keys_of_buckets(bucket_name) |> Poison.decode!
    Enum.map(payload["keys"], fn key -> delete_object_from_key(key, bucket_name) end)
    :ok
  end

  def reset do
    upload_schema("../../../../Resources/chap6/riak/order_schema.xml")
    # remove_bucket()
    remove_index()
    create_index()
    assign_index()
    JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
  end

  def remove_index do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:delete, {'#{Riak.url}/search/index/#{@index_name}', Riak.auth_header()}, [], [])
  end

  def remove_bucket do
    clear_bucket()
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:delete, {'#{Riak.url}/buckets/#{@bucket_name}/props', Riak.auth_header()}, [], [])
  end

  def search(index, query, page \\ 0, rows \\ 30, sort \\ "creation_date_index") do
    pagination = "&start=#{rows*page}&rows=#{rows}"
    # test = String.split(queryy, ":")
    # IO.inspect test
    # query = Enum.at(test, 0) <> ":" <> Riak.escape(Enum.at(test, 1)) <> Riak.escape(Enum.at(test, 2)) <> Riak.escape(Enum.at(test, 3))
    IO.inspect '#{Riak.url}/search/query/#{index}/?wt=json&q=#{query}#{pagination}&sort=#{sort}+asc'
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/search/query/#{index}/?wt=json&q=#{query}#{pagination}&sort=#{sort}+asc', Riak.auth_header()}, [], [])
    # IO.puts body
    body
  end

  def fetch_orders(query \\ "id:*", page \\ 0, rows \\ 30, sort \\ "creation_date_index") do
    payload = search(@index_name, query, page, rows, sort)
    raw_data = Poison.decode!(payload)["response"]["docs"]
    # IO.inspect raw_data
    raw_data |> Enum.map(fn payload ->
      %{
        "id" => payload["id"],
        "status" => payload["status.state"],
        "shipping_method" => payload["custom.shipping_method"],
        "email" => payload["custom.customer.email"],
        "quantity" => payload["custom.magento.total_item_count"],
        "full_name" => payload["custom.customer.full_name"],
      }
    end)
  end

  def fetch() do
    payload = search(@index_name, "id:*")
    raw_data = Poison.decode!(payload)["response"]["docs"]
    # IO.inspect raw_data
    raw_data |> Enum.map(fn payload ->
      %{
        "id" => payload["id"],
        "status" => payload["status.state"],
        "shipping_method" => payload["custom.shipping_method"],
        "email" => payload["custom.customer.email"],
        "quantity" => payload["custom.magento.total_item_count"],
        "full_name" => payload["custom.customer.full_name"],
      }
    end)
  end

  def fetch(id) do
    raw_data = get_object_from_key(id) |> Poison.decode!
    %{
      "customer_name" => raw_data["custom"]["customer"]["full_name"],
      "shipping_address" => raw_data["custom"]["shipping_address"],
      "id" => raw_data["id"],
      "items" => raw_data["custom"]["items"],
    }
  end

  def remove_order(id) do
    raw_data = delete_object_from_key(id)
    IO.inspect raw_data
  end

  # TMP

  def get_schema(schema_name \\ @schema_name) do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/search/schema/#{schema_name}', Riak.auth_header()}, [], [])
    IO.inspect body, [limit: :infinity]
  end

  def get_bucket_prop(bucket_name \\ @bucket_name) do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{bucket_name}/props', Riak.auth_header()}, [], [])
    body
  end

  def get_list_buckets do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets?buckets=true', Riak.auth_header()}, [], [])
    body
  end

  def put_string_in_bucket(data, bucket_name \\ @bucket_name) do
    # data = "salut"
    {:ok, {{_, 201, _message}, _headers, body}} = :httpc.request(:post, {'#{Riak.url}/buckets/#{bucket_name}/keys', Riak.auth_header(), 'text/plain', data}, [], [])
    body
  end

  def put_json_object_to_bucket(json_payload, key, bucket_name \\ @bucket_name) do
    {:ok, {{_, 204, _message}, _headers, _body}} = :httpc.request(:post, {'#{Riak.url}/buckets/#{bucket_name}/keys/#{key}', Riak.auth_header(), 'application/json', json_payload}, [], [])
  end

  def get_keys_of_buckets(bucket_name \\ @bucket_name) do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{bucket_name}/keys?keys=true', Riak.auth_header()}, [], [])
    body
  end

  def get_object_from_key(key, bucket_name \\ @bucket_name) do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{bucket_name}/keys/#{key}', Riak.auth_header()}, [], [])
    # IO.inspect body, [charlists: :as_charlists]
    body
    # :ok
  end

  def delete_object_from_key(key, bucket_name \\ @bucket_name) do
    {:ok, {{_, 204, _message}, _headers, body}} = :httpc.request(:delete, {'#{Riak.url}/buckets/#{bucket_name}/keys/#{key}', Riak.auth_header()}, [], [])
    body
  end

  def escape(str) do
    Regex.split(~r"[^0-9a-zA-Z ]", str, include_captures: true) |>
    Stream.map(fn expr -> if Regex.match?(~r"[^0-9a-zA-Z ]", expr), do: "\\" <> expr , else: expr end) |>
    Enum.join("")
  end

end
