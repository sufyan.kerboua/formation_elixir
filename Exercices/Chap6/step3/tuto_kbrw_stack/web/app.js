require('!!file-loader?name=[name].[ext]!./index.html')
/* required library for our React app */
var ReactDOM = require('react-dom')
var React = require("react")
var createReactClass = require('create-react-class');
const { render } = require('@testing-library/react');
var Qs = require('qs')
var Cookie = require('cookie')
var XMLHttpRequest = require("xhr2")

/* required css for our application */
// require('./webflow/home/home.css');
require('./webflow/order/order.css');
require('./webflow/loader/loader.css');
require('./webflow/deleteModal/deleteModal.css');
require('./webflow/orders/orders.css');

// HTTP calls to requests our data from Elixir
var HTTP = new (function(){
  this.get = (url)=>this.req('GET',url)
  this.delete = (url)=>this.req('DELETE',url)
  this.post = (url,data)=>this.req('POST',url,data)
  this.put = (url,data)=>this.req('PUT',url,data)

  this.req = (method,url,data)=> new Promise((resolve, reject) => {
    var req = new XMLHttpRequest()
    // console.log({"url": url})
    req.open(method, url)
    req.responseType = "text"
    req.setRequestHeader("accept","application/json,*/*;0.8")
    req.setRequestHeader("content-type","application/json")
    req.onload = ()=>{
      if (req.status >= 200 && req.status < 300) {
        // @todo remove log
        // console.log({"req": req.responseText})
        resolve(req.responseText && JSON.parse(req.responseText))
        // resolve(req.responseText)
      } else {
        reject({http_code: req.status})
      }
    }
  req.onerror = (err)=>{
    reject({http_code: req.status})
  }
  req.send(data && JSON.stringify(data))
  })
})()

// Object that contains URLs of our API to request the data 
var remoteProps = {
  orders: (props)=>{
    // console.log({"myyy props": props})
    var query = Qs.stringify(props.qs)
    return {
      url: "/api/orders" + (query == '' ? '' : '?' + query),
      prop: "orders"
    }
  },
  order: (props)=>{
    return {
      url: "/api/order/" + props.order_id,
      prop: "order"
    }
  }
}

function addRemoteProps(props){
  // @todo remove log
  // console.log({"Hello there props": props})

  return new Promise((resolve, reject)=>{
    // Here we could call `[].concat.apply` instead of `Array.prototype.concat.apply`.
    // ".apply" first parameter defines the `this` of the concat function called.
    // Ex: [0,1,2].concat([3,4],[5,6])-> [0,1,2,3,4,5,6]
    // Is the same as : Array.prototype.concat.apply([0,1,2],[[3,4],[5,6]])
    // Also : `var list = [1,2,3]` is the same as `var list = new Array(1,2,3)`
    var remoteProps = Array.prototype.concat.apply([],
      props.handlerPath
        .map((c)=> c.remoteProps) // -> [[remoteProps.orders], null]
        .filter((p)=> p) // -> [[remoteProps.orders]]
    )
    // browserState.handlerPath[i].remoteProps[j]
    // props.handlerPath[i].remoteProps[j]

    remoteProps = remoteProps
      .map((spec_fun)=> spec_fun(props) ) // [{url: '/api/orders', prop: 'orders'}]
      .filter((specs)=> specs) // get rid of undefined from remoteProps that don't match their dependencies
      .filter((specs)=> !props[specs.prop] ||  props[specs.prop].url != specs.url) // get rid of remoteProps already resolved with the url

    if(remoteProps.length == 0)
      return resolve(props)
    
    // All remoteProps can be queried in parallel. This is just the function definition, see its use below.
    const promise_mapper = (spec) => {
      // @todo remove
      // console.log({"Spec": spec})
      // we want to keep the url in the value resolved by the promise here : spec = {url: '/api/orders', value: ORDERS, prop: 'orders'}
      return HTTP.get(spec.url).then((res) => { spec.value = res; return spec })
    }

    const reducer = (acc, spec) => {
      // spec = url: '/api/orders', value: ORDERS, prop: 'user'}
      acc[spec.prop] = {url: spec.url, value: spec.value}
      return acc
    }

    const promise_array = remoteProps.map(promise_mapper)
    return Promise.all(promise_array)
      .then(xs => xs.reduce(reducer, props), reject)
      .then((p) => {
      // recursively call remote props, because props computed from
      // previous queries can give the missing data/props necessary
      // to define another query
      return addRemoteProps(p).then(resolve, reject)
    }, reject)

  })
}

var orders = [
  {remoteid: "000000189", custom: {customer: {full_name: "TOTO & CIE"}, billing_address: "Some where in the world"}, items: 2}, 
  {remoteid: "000000190", custom: {customer: {full_name: "Looney Toons"}, billing_address: "The Warner Bros Company"}, items: 3}, 
  {remoteid: "000000191", custom: {customer: {full_name: "Asterix & Obelix"}, billing_address: "Armorique"}, items: 29}, 
  {remoteid: "000000192", custom: {customer: {full_name: "Lucky Luke"}, billing_address: "A Cowboy doesn't have an address. Sorry"}, items: 0}, 
]

var cn = function(){
  var args = arguments, classes = {}
  for (var i in args) {
    var arg = args[i]
    if(!arg) continue
    if ('string' === typeof arg || 'number' === typeof arg) {
      arg.split(" ").filter((c)=> c!="").map((c)=>{
        classes[c] = true
      })
    } else if ('object' === typeof arg) {
      for (var key in arg) classes[key] = arg[key]
    }
  }
  return Object.keys(classes).map((k)=> classes[k] && k || '').join(' ')
}

var Page = createReactClass({
  render() {
    return (
      <JSXZ in="orders/orders" sel=".container">
        <Z sel=".table_container_body">
          {
            orders.map(order => (
              <JSXZ in="home/home" sel=".table_container_row">
                <Z sel=".text_cn">{order.remoteid}</Z>
                <Z sel=".text_customer">{order.custom.customer.full_name}</Z>
                <Z sel=".text_address">{order.custom.billing_address}</Z>
                <Z sel=".text_quantity">{order.items}</Z>
              </JSXZ>
            ))
          }
        </Z>
      </JSXZ>
    )
  }
})

var Child = createReactClass({
  render(){
    var [ChildHandler, ...rest] = this.props.handlerPath
    return <ChildHandler {...this.props} handlerPath={rest} />
  }
})

var Layout = createReactClass({
  getInitialState: function() {
    return {modal: null, loader: false};
  },
  modal(spec) {
    this.setState({modal: {
      ...spec, callback: (res)=>{
        this.setState({modal: null},()=>{
          if(spec.callback) spec.callback(res)
        })
      }
    }})
  },
  loader(promiseCallback) {
    return new Promise((resolve, reject) => {
      this.setState({loader: true})
      promiseCallback.then((res) => {
        this.setState({loader: false})
        resolve(res)
      })
    })
  },
  render(){
    var modal_component = { 
      'delete': (props) => <DeleteModal {...props}/>
    }[this.state.modal && this.state.modal.type];

    modal_component = modal_component && modal_component(this.state.modal)
    console.log({"Layout state": this})

    var props = {
      ...this.props, modal: this.modal, loader: this.loader
    }

    return <JSXZ in="orders/orders" sel=".layout">
        <Z sel=".modal-wrapper" className={cn(classNameZ, {'hidden': !modal_component})}>
          {modal_component}
        </Z>
        <Z sel=".loader-wrapper" className={cn(classNameZ, {'hidden': !this.state.loader})}>
          <Loader />
        </Z>
        <Z sel=".layout-container">
          <this.props.Child {...props}/>
        </Z>
      </JSXZ>
    }
})

var Header = createReactClass({
  render(){
    return <JSXZ in="orders/orders" sel=".header">
        <Z sel=".header-container">
          <this.props.Child {...this.props}/>
        </Z>
      </JSXZ>
    }
})

var Orders = createReactClass({
  statics: {
    remoteProps: [remoteProps.orders]
  },
  getInitialState: function() {
    return {
      modalArg: {
        type: 'delete',
        title: 'Order deletion',
        message: `Are you sure you want to delete this ?`,
        callback: (value)=>{
          if (value === true) {
            this.props.loader(HTTP.delete(`/api/order/${this.state.modalArg.commandNumber}`))
            .then((res) => {
              browserState = {
                Child: browserState.Child,
                cookie: browserState.cookie,
                handlerPath: browserState.handlerPath,
                path: browserState.path,
                qs: browserState.qs,
                route: browserState.route
              }
              GoTo("orders", null, null)
            })
          }
          return value
        }
      }
    }
  },
  render(){
    console.log({"Props from Layout": this.props})
    return (
      <JSXZ in="orders/orders" sel=".container">
        <Z sel=".table_container_body">
          {
            this.props.orders.value.map(order => 
              (
              <JSXZ in="orders/orders" sel=".table_container_row">
                <Z sel=".text_cn">{order.id}</Z>
                <Z sel=".text_customer">{order.data.custom.customer.full_name}</Z>
                <Z sel=".text_address">{order.data.custom.billing_address.city}</Z>
                <Z sel=".text_quantity">{order.data.custom.items.length}</Z>
                <Z sel=".text_icon" onClick={() => GoTo("order", order.id, null)}>
                  <JSXZ in="orders/orders" sel=".text_icon">
                  </JSXZ>
                </Z>
                <Z sel=".text_pay">Status : {order.data.status.state}<br/>Payment Method : {order.data.custom.shipping_method_ui}</Z>
                <Z sel=".icon_delete" onClick={() => {
                  this.state.modalArg.commandNumber = order.id
                  this.props.modal(this.state.modalArg)
                  }}>
                  <JSXZ in="orders/orders" sel=".icon_delete">
                  </JSXZ>
                </Z>
              </JSXZ>
            ))
          }
        </Z>
      </JSXZ>
    )
    }
})

var Order = createReactClass({
  statics: {
    remoteProps: [remoteProps.order]
  },
  render(){
    let order = this.props.order.value
    let shipping_address = order.data.custom.shipping_address
    let items = order.data.custom.items
    return (
      <JSXZ in="order/order" sel=".container">
        <Z sel=".customer_name">{order.data.custom.customer.full_name}</Z>
        <Z sel=".customer_address">{`${shipping_address.street.join(" ")} ${shipping_address.city} ${shipping_address.postcode}`}</Z>
        <Z sel=".customer_cn">{order.id}</Z>
        <Z sel=".order_table_body">
          {
            items.map(item =>
            (<JSXZ in="order/order" sel=".order_table_row">
              <Z sel=".text_product_name">{item.product_title}</Z>
              <Z sel=".text_quantity">{item.quantity_to_fetch}</Z>
              <Z sel=".text_unit_price">{item.unit_price}</Z>
              <Z sel=".text_total_price">{item.quantity_to_fetch * item.unit_price}</Z>
            </JSXZ>
            ))
          }
          </Z>
      </JSXZ>
    )}
})

var ErrorPage = createReactClass({
  render(){
    return <JSXZ in="loader/loader" sel=".loader-wrapper">
      </JSXZ>
    }
})

var DeleteModal = createReactClass({
  render(){
    return (
    <JSXZ in="deleteModal/deleteModal" sel=".modal-content">
      <Z sel=".modal_text_header">{this.props.message}</Z>
      <Z sel=".modal_btn_yes" onClick={() => this.props.callback(true)}>
        <JSXZ in="deleteModal/deleteModal" sel=".modal_btn_yes"></JSXZ>
      </Z>
      <Z sel=".modal_btn_no" onClick={() => this.props.callback(false)}>
        <JSXZ in="deleteModal/deleteModal" sel=".modal_btn_no"></JSXZ>
      </Z>
    </JSXZ>)
  }
})

var Loader = createReactClass({
  render(){
    return <JSXZ in="loader/loader" sel=".loader-wrapper">
      </JSXZ>
    }
})

var routes = {
  "orders": {
    path: (params) => {
      return "/";
    },
    match: (path, qs) => {
      // return (path == "/") && {handlerPath: [Page]} // Note that we use the "&&" expression to simulate a IF statement
      return (path == "/") && {handlerPath: [Layout, Header, Orders]}
    }
  }, 
  "order": {
    path: (params) => {
      return "/order/" + params;
    },
    match: (path, qs) => {
      var r = new RegExp("/order/([^/]*)$").exec(path)
      return r && {handlerPath: [Layout, Header, Order],  order_id: r[1]} // Note that we use the "&&" expression to simulate a IF statement
    }
  }
}

var GoTo = (route, params, query) => {
  // console.log({"Route": route})
  // console.log({"Params": params})
  // console.log({"Query": query})

  var qs = Qs.stringify(query)
  // console.log({"routes": routes})
  // console.log({"route": route})
  var url = routes[route].path(params) + ((qs=='') ? '' : ('?'+qs))
  history.pushState({}, "", url)
  onPathChange()
}

var browserState = {Child: Child}

function onPathChange() {
  var path = location.pathname
  var qs = Qs.parse(location.search.slice(1))
  var cookies = Cookie.parse(document.cookie)

  browserState = {
    ...browserState, 
    path: path, 
    qs: qs, 
    cookie: cookies
  }
  // @todo remove log
  // console.log({"browserState": browserState})
  // console.log({"routes": routes})

  var route
  // We try to match the requested path to one our our routes
  for (var key in routes) {
    routeProps = routes[key].match(path, qs)
    if (routeProps){
        route = key
          break;
    }
  }

  // GOAL => check the list of components in the HandlerPath and get the static remoteProps list for each of them

  // Call the corresponding URLs on our API, and retrieve the remote data
  // addRemoteProps(browserState)

  // We add the route name and the route Props to the global browserState
  browserState = {
    ...browserState,
    ...routeProps,
    route: route
  }

  // // If the path in the URL doesn't match with any of our routes, we render an Error component (we will have to create it later)
  // if (!route)
  //   return ReactDOM.render(<ErrorPage message={"Not Found"} code={404}/>, document.getElementById('root'))

  // // If we found a match, we render the Child component, which will render the handlerPath components recursively, remember ? ;)
  // ReactDOM.render(<Child {...browserState}/>, document.getElementById('root'));

  addRemoteProps(browserState).then(
    (props) => {
      browserState = props
      // Log our new browserState
      // console.log({"Is this my browser State ??": browserState})
      // Render our components using our remote data
      ReactDOM.render(<Child {...browserState}/>, document.getElementById('root'))
    }, (res) => {
      ReactDOM.render(<ErrorPage message={"Shit happened"} code={res.http_code}/>, document.getElementById('root'))
    })
}

window.addEventListener("popstate", ()=>{ onPathChange() })
onPathChange() // We also call onPathChange once when the js is loaded
