defmodule JsonLoader do

  def load_to_database(database, json_file) do
    with {:ok, body} <- File.read(json_file),
        {:ok, json} <- Poison.decode(body) do

          json |> Enum.map(
            fn element -> Server.Database.push(Server.Database, {element["id"], %{id: element["id"], data: element}}) end
          )

          Server.Database.get(Server.Database)
      end
  end

end
# JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
