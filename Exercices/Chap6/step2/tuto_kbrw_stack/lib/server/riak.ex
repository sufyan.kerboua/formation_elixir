defmodule Riak do
  def url, do: "https://kbrw-sb-tutoex-riak-gateway.kbrw.fr"

  def auth_header do
    username = "sophomore"
    password = "jlessthan3tutoex"
    auth = :base64.encode_to_string("#{username}:#{password}")
    [{'authorization', 'Basic #{auth}'}]
  end

  def get_list_buckets do
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets?buckets=true', Riak.auth_header()}, [], [])
  end

  def put_object_in_bucket do
    bucket = 'sufyan_orders'
    data = '[
      {
        color: "red",
        value: "#f00"
      },
      {
        color: "green",
        value: "#0f0"
      },
      {
        color: "blue",
        value: "#00f"
      },
      {
        color: "cyan",
        value: "#0ff"
      },
      {
        color: "magenta",
        value: "#f0f"
      },
      {
        color: "yellow",
        value: "#ff0"
      },
      {
        color: "black",
        value: "#000"
      }
    ]'
    # {:ok, {{_, 201, _message}, _headers, body}} = :httpc.request(:post, {'#{Riak.url}/buckets/#{bucket}/keys', Riak.auth_header(), 'text/plain', data}, [], [])
    {:ok, {{_, 201, _message}, _headers, body}} = :httpc.request(:post, {'#{Riak.url}/buckets/#{bucket}/keys', Riak.auth_header(), 'application/json', data}, [], [])
  end

  def get_keys_of_buckets do
    bucket = 'sufyan_orders'
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{bucket}/keys?keys=true', Riak.auth_header()}, [], [])
  end

  def get_object_from_key(key) do
    bucket = 'sufyan_orders'
    {:ok, {{_, 200, _message}, _headers, body}} = :httpc.request(:get, {'#{Riak.url}/buckets/#{bucket}/keys/#{key}', Riak.auth_header()}, [], [])
  end

  def delete_object_from_key(key) do
    bucket = 'sufyan_orders'
    {:ok, {{_, 204, _message}, _headers, body}} = :httpc.request(:delete, {'#{Riak.url}/buckets/#{bucket}/keys/#{key}', Riak.auth_header()}, [], [])
  end

end
