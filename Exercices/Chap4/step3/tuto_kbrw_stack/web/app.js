require('!!file-loader?name=[name].[ext]!./index.html')
/* required library for our React app */
var ReactDOM = require('react-dom')
var React = require("react")
var createReactClass = require('create-react-class');
const { render } = require('@testing-library/react');

/* required css for our application */
require('./webflow/home.css');
require('./webflow/order.css');

// require('./webflow/home.js');
// require('./webflow/order.js');

// require('./webflow/fa-solid-900.woff2');
// require('./tuto.webflow/css/webflow.css');
// require('./tuto.webflow/css/tuto.webflow.css');

var orders = [
  {remoteid: "000000189", custom: {customer: {full_name: "TOTO & CIE"}, billing_address: "Some where in the world"}, items: 2}, 
  {remoteid: "000000190", custom: {customer: {full_name: "Looney Toons"}, billing_address: "The Warner Bros Company"}, items: 3}, 
  {remoteid: "000000191", custom: {customer: {full_name: "Asterix & Obelix"}, billing_address: "Armorique"}, items: 29}, 
  {remoteid: "000000192", custom: {customer: {full_name: "Lucky Luke"}, billing_address: "A Cowboy doesn't have an address. Sorry"}, items: 0}, 
]

var Page = createReactClass({
  render() {
    return (
      <JSXZ in="modal/modal" sel=".modal-wrapper">
        {/* <Z sel=".table_container_body">
          {
            orders.map(order => (
              <JSXZ in="home" sel=".table_container_row">
                <Z sel=".text_cn">{order.remoteid}</Z>
                <Z sel=".text_customer">{order.custom.customer.full_name}</Z>
                <Z sel=".text_address">{order.custom.billing_address}</Z>
                <Z sel=".text_quantity">{order.items}</Z>
              </JSXZ>
            ))
          }
        </Z> */}
      </JSXZ>
    )
  }
})

ReactDOM.render(<Page />, document.getElementById('root'));
