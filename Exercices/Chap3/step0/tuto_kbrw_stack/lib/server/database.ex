defmodule Server.Database do
    use GenServer

    @order1 %{id: "toto", key: 42}
    @order2 %{id: "test", key: "42"}
    @order3 %{id: "test", key: "Apero?"}
    @order4 %{id: "kbrw", key: "Ho yeah"}
    @current_index 4
    @table :users

    #Client
    def start_link(_arg) do
        GenServer.start_link(__MODULE__, @current_index, name: __MODULE__)
        # JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
    end

    def setup do
        JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
        JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk1.json")
    end

    def search(pid, criteria) do
        GenServer.call(pid, {:my_search, criteria})
    end

    def get(pid) do
        GenServer.call(pid, :get)
    end

    def get(pid, key) do
        GenServer.call(pid, {:get, key})
    end

    def get_info(pid) do
        GenServer.call(pid, :get_info)
    end

    def push(pid, value) do
        GenServer.cast(pid, {:put, value})
    end

    def delete(pid) do
        GenServer.cast(pid, {:delete})
    end

    def delete(pid, key) do
        GenServer.cast(pid, {:delete, key})
    end

    #Server (CallBacks)
    @impl true
    def init(db_table) do
        :ets.new(:db_table, [:named_table, :public])
        :ets.insert_new(:db_table, {0, @order1})
        :ets.insert_new(:db_table, {1, @order2})
        :ets.insert_new(:db_table, {2, @order3})
        :ets.insert_new(:db_table, {3, @order4})
        {:ok, db_table}
    end

    @impl true
    def handle_cast({:put, value}, intern_state) do
        IO.inspect intern_state
        :ets.insert_new(:db_table, {intern_state + 1, value})
        {:noreply, intern_state + 1}
    end

    @impl true
    def handle_cast({:delete, key}, intern_state) do
        :ets.delete(:db_table, key)
        {:noreply, intern_state}
    end

    @impl true
    def handle_cast({:delete}, intern_state) do
        :ets.delete_all_objects(:db_table)
        {:noreply, intern_state}
    end

    @impl true
    def handle_call(:get, _from, intern_state) do
        IO.inspect intern_state
        full_db = :ets.tab2list(:db_table)
        {:reply, full_db, intern_state}
    end

    @impl true
    def handle_call({:get, key}, _from, intern_state) do
        selected_elem = :ets.lookup(:db_table, key)
        {:reply, selected_elem, intern_state}
    end

    @impl true
    def handle_call(:get_info, _from, intern_state) do
        selected_elem = :ets.info(:db_table)
        {:reply, selected_elem, intern_state}
    end

    @impl true
    def handle_call({:my_search, criteria}, _from, intern_state) do
        all_item = :ets.tab2list(:db_table) |> Enum.map(fn element -> elem(element, 1) end)

        searched_data = criteria |> Enum.flat_map(
            fn element_to_search ->
                {key, value} = element_to_search
                good_list = Enum.filter(all_item, fn current -> current[String.to_atom(key)] == value end)
            end)
        {:reply, searched_data, intern_state}
    end

    # @impl true
    # def handle_call({:search, {key_to_search, value}}, _from, intern_state) do
    #     # IO.inspect value
    #     q = [
    #         {
    #           {:_, %{key_to_search => value}},
    #           [],
    #           [{:element, 2, :"$_"}]
    #         }
    #       ]
    #     selected_elem = :ets.select(:db_table, q)
    #     # IO.inspect selected_elem
    #     {:reply, selected_elem, intern_state}
    # end
end
