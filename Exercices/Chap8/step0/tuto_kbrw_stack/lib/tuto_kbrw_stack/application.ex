defmodule TutoKbrwStack.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do

    Application.put_env(
      :reaxt,:global_config,
      Map.merge(
        Application.get_env(:reaxt,:global_config), %{localhost: "http://localhost:4001"}
      )
    )
    Reaxt.reload

    children = [
      Server.Database,
      { Plug.Cowboy, scheme: :http, plug: Server.Router, options: [port: 4001] }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TutoKbrwStack.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
