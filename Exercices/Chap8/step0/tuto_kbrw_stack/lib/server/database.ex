defmodule Server.Database do
    use GenServer
    require Riak

    #Client
    def start_link(_arg) do
        GenServer.start_link(__MODULE__, [], name: __MODULE__)
    end

    def setup do
        JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
        # JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk1.json")
    end

    def search(pid, {query, page, rows, sort}) do
        query = if String.strip("") == query, do: "id:*", else: query
        page = if is_nil(page), do: 0, else: String.to_integer(List.last(page))
        rows = if is_nil(rows), do: 30, else: String.to_integer(List.last(rows))
        sort = if is_nil(sort), do: "creation_date_index", else: List.last(sort)
        GenServer.call(pid, {:my_search, {query, page, rows, sort}})
    end

    def get(pid) do
        GenServer.call(pid, :get)
    end

    def get(pid, key) do
        GenServer.call(pid, {:get, key})
    end

    def delete(pid) do
        GenServer.cast(pid, {:delete})
    end

    def delete(pid, key) do
        GenServer.cast(pid, {:delete, key})
    end

    #Server (CallBacks)
    @impl true
    def init(db_table) do
        {:ok, db_table}
    end

    @impl true
    def handle_cast({:delete, key}, intern_state) do
        Riak.remove_order(key)
        {:noreply, intern_state}
    end

    @impl true
    def handle_cast({:delete}, intern_state) do
        Riak.clear_bucket()
        {:noreply, intern_state}
    end

    @impl true
    def handle_call(:get, _from, intern_state) do
        full_db = Riak.fetch()
        {:reply, full_db, intern_state}
    end

    @impl true
    def handle_call({:get, key}, _from, intern_state) do
        selected_elem = Riak.fetch(key)
        {:reply, selected_elem, intern_state}
    end

    @impl true
    def handle_call({:my_search, {query, page, rows, sort}}, _from, intern_state) do
        searched_data = Riak.fetch_orders(query, page, rows, sort)
        {:reply, searched_data, intern_state}
    end
end
