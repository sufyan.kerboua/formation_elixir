defimpl ExFSM.Machine.State, for: Map do
  def state_name(order), do: String.to_atom(order["status"]["state"])
  def set_state_name(order, name), do: put_in(order, ["status", "state"], Atom.to_string(name))
  def handlers(order) do
    [OrderFSM]
  end
end

defmodule OrderFSM do
  use ExFSM
  use GenServer, restart: :transient
  @timer_stop

  # Parent
  def start_link(order_id) do
    GenServer.start_link(__MODULE__, order_id, name: String.to_atom(order_id))
  end

  def update_order(order_id) do
    GenServer.call(String.to_atom(order_id), :update_state)
  end

  deftrans init({:process_payment, []}, order) do
    {:next_state, :not_verified, order}
  end

  deftrans not_verified({:verfication, []}, order) do
    {:next_state, :finished, order}
  end

  # Child
  @impl true
  def init(order_id) do
    {:ok, order_id, :timer.minutes(5)}
  end

  @impl true
  def handle_call(:update_state, _from, intern_order_id) do
    order = Riak.get_object_from_key(intern_order_id) |> Poison.decode!

    res =
      case order["status"]["state"] do
        "init" ->
          {:next_state, updated_order} = ExFSM.Machine.event(order, {:process_payment, []})
          Riak.put_json_object_to_bucket(Poison.encode!(updated_order), updated_order["id"])
          updated_order
        "not_verified" ->
          {:next_state, updated_order} = ExFSM.Machine.event(order, {:verfication, []})
          Riak.put_json_object_to_bucket(Poison.encode!(updated_order), updated_order["id"])
          updated_order
        _ ->
          :action_unavailable
      end
    {:reply, res, intern_order_id, :timer.minutes(5)}
  end

  @impl true
  def handle_info(:timeout, _order_id) do
    IO.inspect "GOING to Exit"
    {:stop, :shutdown, []}
  end

end
