require('!!file-loader?name=[name].[ext]!../index.html')
/* required library for our React app */
var ReactDOM = require('react-dom')
var React = require("react")
var createReactClass = require('create-react-class');
const { render } = require('@testing-library/react');
var Qs = require('qs')
var Cookie = require('cookie')
var localhost = require('reaxt/config').localhost
var XMLHttpRequest = require("xhr2")

/* required css for our application */
// require('./webflow/home/home.css');
require('../webflow/order/order.css');
require('../webflow/loader/loader.css');
require('../webflow/deleteModal/deleteModal.css');
require('../webflow/orders/orders.css');

// HTTP calls to requests our data from Elixir
var HTTP = new (function(){
  this.get = (url)=>this.req('GET',url)
  this.delete = (url)=>this.req('DELETE',url)
  this.post = (url,data)=>this.req('POST',url,data)
  this.put = (url,data)=>this.req('PUT',url,data)

  this.req = (method,url,data)=> {
    
    return new Promise((resolve, reject) => {
    var req = new XMLHttpRequest()
    url = (typeof window !== 'undefined') ? url : localhost+url
    // console.log({"url": url})
    req.open(method, url)
    req.responseType = "text"
    req.setRequestHeader("accept","application/json,*/*;0.8")
    req.setRequestHeader("content-type","application/json")
    req.onload = ()=>{
      if (req.status >= 200 && req.status < 300) {
        // @todo remove log
        // console.log({"req": req.responseText})
        resolve(req.responseText && JSON.parse(req.responseText))
        // resolve(req.responseText)
      } else {
        reject({http_code: req.status})
      }
    }
  req.onerror = (err)=>{
    reject({http_code: req.status})
  }
  req.send(data && JSON.stringify(data))
  })
}
})()


var Link = createReactClass({
  statics: {
    renderFunc: null, //render function to use (differently set depending if we are server sided or client sided)
    GoTo(route, params, query){// function used to change the path of our browser
      console.log('Hello')
      var path = routes[route].path(params)
      var qs = Qs.stringify(query)
      var url = path + (qs == '' ? '' : '?' + qs)
      history.pushState({},"",url)
      Link.onPathChange()
    },
  onPathChange(){ //Updated onPathChange
    var path = location.pathname
    var qs = Qs.parse(location.search.slice(1))
    var cookies = Cookie.parse(document.cookie)
    inferPropsChange(path, qs, cookies).then( //inferPropsChange download the new props if the url query changed as done previously
      ()=>{
        Link.renderFunc(<Child {...browserState}/>) //if we are on server side we render 
      },({http_code})=>{
        Link.renderFunc(<ErrorPage message={"Not Found"} code={http_code}/>, http_code) //idem
      }
    )
  },
  LinkTo: (route,params,query)=> {
    var qs = Qs.stringify(query)
    return routes[route].path(params) +((qs=='') ? '' : ('?'+qs))
  },
},
onClick(ev) {
    ev.preventDefault();
    Link.GoTo(this.props.to,this.props.params,this.props.query);
  },
  render (){//render a <Link> this way transform link into href path which allows on browser without javascript to work perfectly on the website
    return (
      <a href={Link.LinkTo(this.props.to,this.props.params,this.props.query)} onClick={this.onClick}>
        {this.props.children}
      </a>
    )
  }
})

// Object that contains URLs of our API to request the data 
var remoteProps = {
  orders: (props)=>{
    // console.log({"myyy props": props})
    var query = Qs.stringify(props.qs)
    return {
      url: "/api/orders" + (query == '' ? '' : '?' + query),
      prop: "orders"
    }
  },
  order: (props)=>{
    return {
      url: "/api/order/" + props.order_id,
      prop: "order"
    }
  }
}

function addRemoteProps(props){
  // @todo remove log
  // console.log({"Hello there props": props})

  return new Promise((resolve, reject)=>{
    // Here we could call `[].concat.apply` instead of `Array.prototype.concat.apply`.
    // ".apply" first parameter defines the `this` of the concat function called.
    // Ex: [0,1,2].concat([3,4],[5,6])-> [0,1,2,3,4,5,6]
    // Is the same as : Array.prototype.concat.apply([0,1,2],[[3,4],[5,6]])
    // Also : `var list = [1,2,3]` is the same as `var list = new Array(1,2,3)`
    var remoteProps = Array.prototype.concat.apply([],
      props.handlerPath
        .map((c)=> c.remoteProps) // -> [[remoteProps.orders], null]
        .filter((p)=> p) // -> [[remoteProps.orders]]
    )
    // browserState.handlerPath[i].remoteProps[j]
    // props.handlerPath[i].remoteProps[j]

    remoteProps = remoteProps
      .map((spec_fun)=> spec_fun(props) ) // [{url: '/api/orders', prop: 'orders'}]
      .filter((specs)=> specs) // get rid of undefined from remoteProps that don't match their dependencies
      .filter((specs)=> !props[specs.prop] ||  props[specs.prop].url != specs.url) // get rid of remoteProps already resolved with the url

    if(remoteProps.length == 0)
      return resolve(props)
    
    // All remoteProps can be queried in parallel. This is just the function definition, see its use below.
    const promise_mapper = (spec) => {
      // @todo remove
      // console.log({"Spec": spec})
      // we want to keep the url in the value resolved by the promise here : spec = {url: '/api/orders', value: ORDERS, prop: 'orders'}
      return HTTP.get(spec.url).then((res) => { spec.value = res; return spec })
    }

    const reducer = (acc, spec) => {
      // spec = url: '/api/orders', value: ORDERS, prop: 'user'}
      acc[spec.prop] = {url: spec.url, value: spec.value}
      return acc
    }

    const promise_array = remoteProps.map(promise_mapper)
    return Promise.all(promise_array)
      .then(xs => xs.reduce(reducer, props), reject)
      .then((p) => {
      // recursively call remote props, because props computed from
      // previous queries can give the missing data/props necessary
      // to define another query
      return addRemoteProps(p).then(resolve, reject)
    }, reject)

  })
}

var orders = [
  {remoteid: "000000189", custom: {customer: {full_name: "TOTO & CIE"}, billing_address: "Some where in the world"}, items: 2}, 
  {remoteid: "000000190", custom: {customer: {full_name: "Looney Toons"}, billing_address: "The Warner Bros Company"}, items: 3}, 
  {remoteid: "000000191", custom: {customer: {full_name: "Asterix & Obelix"}, billing_address: "Armorique"}, items: 29}, 
  {remoteid: "000000192", custom: {customer: {full_name: "Lucky Luke"}, billing_address: "A Cowboy doesn't have an address. Sorry"}, items: 0}, 
]

var cn = function(){
  var args = arguments, classes = {}
  for (var i in args) {
    var arg = args[i]
    if(!arg) continue
    if ('string' === typeof arg || 'number' === typeof arg) {
      arg.split(" ").filter((c)=> c!="").map((c)=>{
        classes[c] = true
      })
    } else if ('object' === typeof arg) {
      for (var key in arg) classes[key] = arg[key]
    }
  }
  return Object.keys(classes).map((k)=> classes[k] && k || '').join(' ')
}

var Page = createReactClass({
  render() {
    return (
      <JSXZ in="orders/orders" sel=".container">
        <Z sel=".table_container_body">
          {
            orders.map(order => (
              <JSXZ in="home/home" sel=".table_container_row">
                <Z sel=".text_cn">{order.remoteid}</Z>
                <Z sel=".text_customer">{order.custom.customer.full_name}</Z>
                <Z sel=".text_address">{order.custom.billing_address}</Z>
                <Z sel=".text_quantity">{order.items}</Z>
              </JSXZ>
            ))
          }
        </Z>
      </JSXZ>
    )
  }
})

var Child = createReactClass({
  render(){
    var [ChildHandler, ...rest] = this.props.handlerPath
    return <ChildHandler {...this.props} handlerPath={rest} />
  }
})

var Layout = createReactClass({
  getInitialState: function() {
    return {modal: null, loader: false};
  },
  modal(spec) {
    this.setState({modal: {
      ...spec, callback: (res)=>{
        this.setState({modal: null},()=>{
          if(spec.callback) spec.callback(res)
        })
      }
    }})
  },
  loader(promiseCallback) {
    return new Promise((resolve, reject) => {
      this.setState({loader: true})
      promiseCallback.then((res) => {
        this.setState({loader: false})
        resolve(res)
      })
    })
  },
  render(){
    var modal_component = { 
      'delete': (props) => <DeleteModal {...props}/>
    }[this.state.modal && this.state.modal.type];

    modal_component = modal_component && modal_component(this.state.modal)
    console.log({"Layout state": this})

    var props = {
      ...this.props, modal: this.modal, loader: this.loader
    }

    return <JSXZ in="orders/orders" sel=".layout">
        <Z sel=".modal-wrapper" className={cn(classNameZ, {'hidden': !modal_component})}>
          {modal_component}
        </Z>
        <Z sel=".loader-wrapper" className={cn(classNameZ, {'hidden': !this.state.loader})}>
          <Loader />
        </Z>
        <Z sel=".layout-container">
          <this.props.Child {...props}/>
        </Z>
      </JSXZ>
    }
})

var Header = createReactClass({
  render(){
    return <JSXZ in="orders/orders" sel=".header">
        <Z sel=".header-container">
          <this.props.Child {...this.props}/>
        </Z>
      </JSXZ>
    }
})

var Orders = createReactClass({
  statics: {
    remoteProps: [remoteProps.orders]
  },
  getInitialState: function() {
    return {
      modalArg: {
        type: 'delete',
        title: 'Order deletion',
        message: `Are you sure you want to delete this ?`,
        callback: (value)=>{
          if (value === true) {
            this.props.loader(HTTP.delete(`/api/order/${this.state.modalArg.commandNumber}`))
            .then((res) => {
              browserState = {
                Child: browserState.Child,
                cookie: browserState.cookie,
                handlerPath: browserState.handlerPath,
                path: browserState.path,
                qs: browserState.qs,
                route: browserState.route
              }
              Link.GoTo("orders", null, null)
            })
          }
          return value
        }
      },
      searchInput: ""
    }
  },
  handleInputChange(e) {
    const myValue = e.target.value;
    this.setState({
      searchInput: myValue
    })
  },
  render(){
    console.log({"Props from Layout": this.props})
    let currentPage = this.props.qs.page ? parseInt(this.props.qs.page) : 0
    console.log({"Current Page": currentPage})
    return (
      <JSXZ in="orders/orders" sel=".container">
        <Z sel=".search_form_container">
          <JSXZ in="orders/orders" sel=".search_form_body">
            <Z sel=".search_input" value={this.state.searchInput} onChange={e => this.handleInputChange(e)}></Z>
            <Z sel=".search_btn" onClick={() => {
              payload = this.state.searchInput.split(":")
              if (payload.length === 2) Link.GoTo("orders", null, {[payload[0]]: payload[1]})
            }}>Submit</Z>
          </JSXZ>
        </Z>
        <Z sel=".table_container_body">
          {
            this.props.orders.value.map(order => 
              (
              <JSXZ in="orders/orders" sel=".table_container_row" key={order.id}>
                <Z sel=".text_cn">{order.id}</Z>
                <Z sel=".text_customer">{order.full_name}</Z>
                <Z sel=".text_address">{order.email}</Z>
                <Z sel=".text_quantity">{order.quantity}</Z>
                <Z sel=".text_icon" onClick={() => Link.GoTo("order", order.id, null)}>
                  <JSXZ in="orders/orders" sel=".text_icon">
                  </JSXZ>
                </Z>
                <Z sel=".icon_pay" onClick={() => {
                  this.props.loader(HTTP.get(`/api/pay/${order.id}`))
                  .then((res) => {
                    browserState = {
                      Child: browserState.Child,
                      cookie: browserState.cookie,
                      handlerPath: browserState.handlerPath,
                      path: browserState.path,
                      qs: browserState.qs,
                      route: browserState.route
                    }
                    console.log(res)
                    if (res.error) {
                      alert(res.error)
                    }
                    Link.GoTo("orders", null, null)
                  })
                }}>
                  <JSXZ in="orders/orders" sel=".icon_pay">
                  </JSXZ>
                </Z>
                <Z sel=".text_pay">Status : {order.status}<br/>Payment Method : {order.shipping_method}</Z>
                <Z sel=".icon_delete" onClick={() => {
                  this.state.modalArg.commandNumber = order.id
                  this.props.modal(this.state.modalArg)
                  }}>
                  <JSXZ in="orders/orders" sel=".icon_delete">
                  </JSXZ>
                </Z>
              </JSXZ>
            ))
          }
        </Z>
        <Z sel=".navigation">
          {
            currentPage !== 0 ?
            <JSXZ in="orders/orders" sel=".nav_item">
              <Z sel=".link_nav_page" onClick={() => Link.GoTo("orders", null, {"page": currentPage - 1})}>{currentPage - 1}</Z>
            </JSXZ> : null
          }
          <JSXZ in="orders/orders" sel=".nav_item">
            <Z sel=".link_nav_page" onClick={() => Link.GoTo("orders", null, {"page": currentPage})}>{currentPage}</Z>
          </JSXZ>
          <JSXZ in="orders/orders" sel=".nav_item">
            <Z sel=".link_nav_page" onClick={() => Link.GoTo("orders", null, {"page": currentPage + 1})}>{currentPage + 1}</Z>
          </JSXZ>
        </Z>
      </JSXZ>
    )
  }
})

var Order = createReactClass({
  statics: {
    remoteProps: [remoteProps.order]
  },
  render(){
    let order = this.props.order.value
    let shipping_address = order.shipping_address
    let items = order.items
    return (
      <JSXZ in="order/order" sel=".container">
        <Z sel=".customer_name">{order.customer_name}</Z>
        <Z sel=".customer_address">{`${shipping_address.street.join(" ")} ${shipping_address.city} ${shipping_address.postcode}`}</Z>
        <Z sel=".customer_cn">{order.id}</Z>
        <Z sel=".order_table_body">
          {
            items.map(item =>
            (<JSXZ in="order/order" sel=".order_table_row" key={item.product_title}>
              <Z sel=".text_product_name">{item.product_title}</Z>
              <Z sel=".text_quantity">{item.quantity_to_fetch}</Z>
              <Z sel=".text_unit_price">{item.unit_price}</Z>
              <Z sel=".text_total_price">{item.quantity_to_fetch * item.unit_price}</Z>
            </JSXZ>
            ))
          }
          </Z>
      </JSXZ>
    )}
})

var ErrorPage = createReactClass({
  render(){
    return <JSXZ in="loader/loader" sel=".loader-wrapper">
      </JSXZ>
    }
})

var DeleteModal = createReactClass({
  render(){
    return (
    <JSXZ in="deleteModal/deleteModal" sel=".modal-content">
      <Z sel=".modal_text_header">{this.props.message}</Z>
      <Z sel=".modal_container_btn:first-of-type" onClick={() => this.props.callback(true)}>
        <JSXZ in="deleteModal/deleteModal" sel=".modal_btn_yes"></JSXZ>
      </Z>
      <Z sel=".modal_container_btn:last-of-type" onClick={() => this.props.callback(false)}>
        <JSXZ in="deleteModal/deleteModal" sel=".modal_btn_no"></JSXZ>
      </Z>
    </JSXZ>)
  }
})

var Loader = createReactClass({
  render(){
    return <JSXZ in="loader/loader" sel=".loader-wrapper">
      </JSXZ>
    }
})

var routes = {
  "orders": {
    path: (params) => {
      return "/";
    },
    match: (path, qs) => {
      // return (path == "/") && {handlerPath: [Page]} // Note that we use the "&&" expression to simulate a IF statement
      return (path == "/") && {handlerPath: [Layout, Header, Orders]}
    }
  }, 
  "order": {
    path: (params) => {
      return "/order/" + params;
    },
    match: (path, qs) => {
      var r = new RegExp("/order/([^/]*)$").exec(path)
      return r && {handlerPath: [Layout, Header, Order],  order_id: r[1]} // Note that we use the "&&" expression to simulate a IF statement
    }
  }
}

var browserState = {}

function inferPropsChange(path,query,cookies){ // the second part of the onPathChange function have been moved here
  browserState = {
    ...browserState,
    path: path, qs: query,
    Link: Link,
    Child: Child
  }

  var route, routeProps
  for(var key in routes) {
    routeProps = routes[key].match(path, query)
    if(routeProps){
      route = key
      break
    }
  }

  if(!route){
    return new Promise( (res,reject) => reject({http_code: 404}))
  }
  browserState = {
    ...browserState,
    ...routeProps,
    route: route
  }

  return addRemoteProps(browserState).then(
    (props)=>{
      browserState = props
    })
}

export default {
  reaxt_server_render(params, render){
    inferPropsChange(params.path, params.query, params.cookies)
      .then(()=>{
        render(<Child {...browserState}/>)
      },(err)=>{
        render(<ErrorPage message={"Not Found :" + err.url } code={err.http_code}/>, err.http_code)
      })
  },
  reaxt_client_render(initialProps, render){
    browserState = initialProps
    Link.renderFunc = render
    window.addEventListener("popstate", ()=>{ Link.onPathChange() })
    Link.onPathChange()
  }
}