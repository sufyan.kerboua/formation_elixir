defmodule Server.Database do
    use GenServer

    #Client
    def start_link(_arg) do
        :ets.new(:db_table, [:named_table, :public])
        # :ets.insert_new(:db_table, {%{"id" => "toto", "key" => 42}})
        # :ets.insert_new(:db_table, {%{"id" => "test", "key" => "42"}})
        # :ets.insert_new(:db_table, {%{"id" => "tata", "key" => "Apero?"}})
        # :ets.insert_new(:db_table, {%{"id" => "kbrw", "key" => "Oh yeah"}})

        :ets.insert_new(:db_table, {"toto", 42})
        :ets.insert_new(:db_table, {"test", "42"})
        :ets.insert_new(:db_table, {"tata", "Apero?"})
        :ets.insert_new(:db_table, {"kbrw", "Oh yeah"})

        GenServer.start_link(__MODULE__, [], name: __MODULE__)
        # JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
    end

    def setup do
        JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk0.json")
        JsonLoader.load_to_database(Server.Database, "orders_dump/orders_chunk1.json")
    end

    def search(pid, criteria) do
        Enum.map(
            criteria,
            fn payload ->
                case elem(payload, 0) do
                    "id" ->
                        GenServer.call(pid, {:search_id, elem(payload, 1)})
                    _ ->
                        []
                    # "key" ->
                    #     GenServer.call(pid, {:search_key, elem(payload, 1)})
                end
                # ets:match(:db_table, {"toto", '$1'})
            end) |> Enum.filter(fn element -> element !== [] end)
        # GenServer.cast(pis, )
    end

    def get(pid) do
        GenServer.call(pid, :get)
    end

    def get(pid, key) do
        GenServer.call(pid, {:get, key})
    end

    def get_info(pid) do
        GenServer.call(pid, :get_info)
    end

    def push(pid, {key, value}) do
        GenServer.cast(pid, {:put, key, value})
    end

    def delete(pid) do
        GenServer.cast(pid, {:delete})
    end

    def delete(pid, key) do
        GenServer.cast(pid, {:delete, key})
    end

    #Server (CallBacks)
    @impl true
    def init(db_table) do
        {:ok, db_table}
    end

    @impl true
    def handle_cast({:put, key, value}, intern_state) do
        :ets.insert_new(:db_table, {key, value})
        {:noreply, intern_state}
    end

    @impl true
    def handle_cast({:delete, key}, intern_state) do
        :ets.delete(:db_table, key)
        {:noreply, intern_state}
    end

    @impl true
    def handle_cast({:delete}, intern_state) do
        :ets.delete_all_objects(:db_table)
        {:noreply, intern_state}
    end

    @impl true
    def handle_call(:get, _from, intern_table) do
        full_db = :ets.tab2list(:db_table)
        {:reply, full_db, intern_table}
    end

    @impl true
    def handle_call({:get, key}, _from, intern_table) do
        selected_elem = :ets.lookup(:db_table, key)
        {:reply, selected_elem, intern_table}
    end

    @impl true
    def handle_call(:get_info, _from, intern_table) do
        selected_elem = :ets.info(:db_table)
        {:reply, selected_elem, intern_table}
    end

    @impl true
    def handle_call({:search_id, id}, _from, intern_table) do
        selected_elem = :ets.lookup(:db_table, id)
        # IO.inspect selected_elem
        {:reply, selected_elem, intern_table}
    end

    # @impl true
    # def handle_call({:search_key, key}, _from, intern_table) do
    #     selected_elem = :ets.select(:db_table, '$1')
    #     IO.inspect selected_elem
    #     {:reply, selected_elem, intern_table}
    # end
end
