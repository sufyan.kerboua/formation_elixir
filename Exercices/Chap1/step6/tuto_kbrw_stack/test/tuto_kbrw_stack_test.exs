defmodule Server.Database.Test do
  use ExUnit.Case
  doctest Server.Database

  test "should get full table" do
    assert Server.Database.get(Server.Database) == [{4, "Again"}, {1, "Here"}, {3, "Go"}, {0, "Shit"}, {2, "We"}]
  end

  test "should add element" do
    assert Server.Database.push(Server.Database, {5, "yaaa"}) == :ok
    assert Server.Database.push(Server.Database, {7, "my"}) == :ok
    assert Server.Database.delete(Server.Database) == :ok
  end

  test "should get a specific row" do
    assert Server.Database.get(Server.Database, 4) == [{4, "Again"}]
  end

end
