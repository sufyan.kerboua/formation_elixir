defmodule Server.Database do
    use GenServer

    #Client
    def start_link(_arg) do
        :ets.new(:db_table, [:named_table, :public])
        GenServer.start_link(__MODULE__, _arg, name: __MODULE__)
    end

    def get(pid) do
        GenServer.call(pid, :get)
    end

    def get(pid, key) do
        GenServer.call(pid, {:get, key})
    end

    def get_info(pid) do
        GenServer.call(pid, :get_info)
    end

    def push(pid, {key, value}) do
        GenServer.cast(pid, {:put, key, value})
    end

    def delete(pid) do
        GenServer.cast(pid, {:delete})
    end

    def delete(pid, key) do
        GenServer.cast(pid, {:delete, key})
    end

    #Server (CallBacks)
    @impl true
    def init(db_table) do
        {:ok, db_table}
    end

    @impl true
    def handle_cast({:put, key, value}, intern_state) do
        :ets.insert_new(:db_table, {key, value})
        {:noreply, intern_state}
    end

    @impl true
    def handle_cast({:delete, key}, intern_state) do
        :ets.delete(:db_table, key)
        {:noreply, intern_state}
    end

    @impl true
    def handle_cast({:delete}, intern_state) do
        :ets.delete_all_objects(:db_table)
        {:noreply, intern_state}
    end

    @impl true
    def handle_call(:get, _from, intern_table) do
        full_db = :ets.tab2list(:db_table)
        {:reply, full_db, intern_table}
    end

    @impl true
    def handle_call({:get, key}, _from, intern_table) do
        selected_elem = :ets.lookup(:db_table, key)
        {:reply, selected_elem, intern_table}
    end

    @impl true
    def handle_call(:get_info, _from, intern_table) do
        selected_elem = :ets.info(:db_table)
        {:reply, selected_elem, intern_table}
    end
    :get_keys
end
