defmodule Server.TheCreator do

  defmacro __using__(_opts) do
    quote do
      import Plug.Conn

      @endpoints []
      @error {404, "Go away, you are not welcome here"}

      @before_compile Server.TheCreator
    end
  end

  defmacro __before_compile__(_env) do
    quote do

      def init(_arg) do
        IO.inspect @endpoints
        IO.inspect @error
      end

      def call(conn, _opts) do
        endpoint = Enum.filter(
          @endpoints,
          fn {endpoint, _payload} ->
            if endpoint == conn.request_path, do: true
          end
        ) |> Enum.at(0)

        IO.inspect endpoint

        case endpoint do
          nil ->
            {code, msg} = @error
            send_resp(conn, code, msg)
          _ ->
            {code, msg} = elem(endpoint, 1)
            send_resp(conn, code, msg)
        end
      end

    end
  end

  defmacro my_error(code, content) do
    quote do
      @error {unquote(code), unquote(content)}
    end
  end

  defmacro my_get(endpoint, do: arg) do
    quote do
      @endpoints [unquote({endpoint, arg}) | @endpoints]
    end
  end

end
