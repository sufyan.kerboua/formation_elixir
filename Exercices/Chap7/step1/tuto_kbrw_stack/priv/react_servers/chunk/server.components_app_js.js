exports.id = "components_app_js";
exports.ids = ["components_app_js"];
exports.modules = {

/***/ "./components/app.js":
/*!***************************!*\
  !*** ./components/app.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _toArray(arr) { return _arrayWithHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

__webpack_require__(/*! !!file-loader?name=[name].[ext]!../index.html */ "./node_modules/file-loader/dist/cjs.js?name=[name].[ext]!./index.html");
/* required library for our React app */


var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var createReactClass = __webpack_require__(/*! create-react-class */ "./node_modules/create-react-class/index.js");

var _require = __webpack_require__(/*! @testing-library/react */ "./node_modules/@testing-library/react/dist/@testing-library/react.esm.js"),
    render = _require.render;

var Qs = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");

var Cookie = __webpack_require__(/*! cookie */ "./node_modules/cookie/index.js");

var XMLHttpRequest = __webpack_require__(/*! xhr2 */ "./node_modules/xhr2/lib/xhr2.js");
/* required css for our application */
// require('./webflow/home/home.css');


__webpack_require__(Object(function webpackMissingModule() { var e = new Error("Cannot find module '../webflow/order/order.css'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));

__webpack_require__(Object(function webpackMissingModule() { var e = new Error("Cannot find module '../webflow/loader/loader.css'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));

__webpack_require__(Object(function webpackMissingModule() { var e = new Error("Cannot find module '../webflow/deleteModal/deleteModal.css'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));

__webpack_require__(Object(function webpackMissingModule() { var e = new Error("Cannot find module '../webflow/orders/orders.css'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())); // HTTP calls to requests our data from Elixir


var HTTP = new function () {
  var _this = this;

  this.get = function (url) {
    return _this.req('GET', url);
  };

  this.delete = function (url) {
    return _this.req('DELETE', url);
  };

  this.post = function (url, data) {
    return _this.req('POST', url, data);
  };

  this.put = function (url, data) {
    return _this.req('PUT', url, data);
  };

  this.req = function (method, url, data) {
    return new Promise(function (resolve, reject) {
      var req = new XMLHttpRequest(); // console.log({"url": url})

      req.open(method, url);
      req.responseType = "text";
      req.setRequestHeader("accept", "application/json,*/*;0.8");
      req.setRequestHeader("content-type", "application/json");

      req.onload = function () {
        if (req.status >= 200 && req.status < 300) {
          // @todo remove log
          // console.log({"req": req.responseText})
          resolve(req.responseText && JSON.parse(req.responseText)); // resolve(req.responseText)
        } else {
          reject({
            http_code: req.status
          });
        }
      };

      req.onerror = function (err) {
        reject({
          http_code: req.status
        });
      };

      req.send(data && JSON.stringify(data));
    });
  };
}(); // Object that contains URLs of our API to request the data 

var remoteProps = {
  orders: function orders(props) {
    // console.log({"myyy props": props})
    var query = Qs.stringify(props.qs);
    return {
      url: "/api/orders" + (query == '' ? '' : '?' + query),
      prop: "orders"
    };
  },
  order: function order(props) {
    return {
      url: "/api/order/" + props.order_id,
      prop: "order"
    };
  }
};

function addRemoteProps(props) {
  // @todo remove log
  // console.log({"Hello there props": props})
  return new Promise(function (resolve, reject) {
    // Here we could call `[].concat.apply` instead of `Array.prototype.concat.apply`.
    // ".apply" first parameter defines the `this` of the concat function called.
    // Ex: [0,1,2].concat([3,4],[5,6])-> [0,1,2,3,4,5,6]
    // Is the same as : Array.prototype.concat.apply([0,1,2],[[3,4],[5,6]])
    // Also : `var list = [1,2,3]` is the same as `var list = new Array(1,2,3)`
    var remoteProps = Array.prototype.concat.apply([], props.handlerPath.map(function (c) {
      return c.remoteProps;
    }) // -> [[remoteProps.orders], null]
    .filter(function (p) {
      return p;
    }) // -> [[remoteProps.orders]]
    ); // browserState.handlerPath[i].remoteProps[j]
    // props.handlerPath[i].remoteProps[j]

    remoteProps = remoteProps.map(function (spec_fun) {
      return spec_fun(props);
    }) // [{url: '/api/orders', prop: 'orders'}]
    .filter(function (specs) {
      return specs;
    }) // get rid of undefined from remoteProps that don't match their dependencies
    .filter(function (specs) {
      return !props[specs.prop] || props[specs.prop].url != specs.url;
    }); // get rid of remoteProps already resolved with the url

    if (remoteProps.length == 0) return resolve(props); // All remoteProps can be queried in parallel. This is just the function definition, see its use below.

    var promise_mapper = function promise_mapper(spec) {
      // @todo remove
      // console.log({"Spec": spec})
      // we want to keep the url in the value resolved by the promise here : spec = {url: '/api/orders', value: ORDERS, prop: 'orders'}
      return HTTP.get(spec.url).then(function (res) {
        spec.value = res;
        return spec;
      });
    };

    var reducer = function reducer(acc, spec) {
      // spec = url: '/api/orders', value: ORDERS, prop: 'user'}
      acc[spec.prop] = {
        url: spec.url,
        value: spec.value
      };
      return acc;
    };

    var promise_array = remoteProps.map(promise_mapper);
    return Promise.all(promise_array).then(function (xs) {
      return xs.reduce(reducer, props);
    }, reject).then(function (p) {
      // recursively call remote props, because props computed from
      // previous queries can give the missing data/props necessary
      // to define another query
      return addRemoteProps(p).then(resolve, reject);
    }, reject);
  });
}

var orders = [{
  remoteid: "000000189",
  custom: {
    customer: {
      full_name: "TOTO & CIE"
    },
    billing_address: "Some where in the world"
  },
  items: 2
}, {
  remoteid: "000000190",
  custom: {
    customer: {
      full_name: "Looney Toons"
    },
    billing_address: "The Warner Bros Company"
  },
  items: 3
}, {
  remoteid: "000000191",
  custom: {
    customer: {
      full_name: "Asterix & Obelix"
    },
    billing_address: "Armorique"
  },
  items: 29
}, {
  remoteid: "000000192",
  custom: {
    customer: {
      full_name: "Lucky Luke"
    },
    billing_address: "A Cowboy doesn't have an address. Sorry"
  },
  items: 0
}];

var cn = function cn() {
  var args = arguments,
      classes = {};

  for (var i in args) {
    var arg = args[i];
    if (!arg) continue;

    if ('string' === typeof arg || 'number' === typeof arg) {
      arg.split(" ").filter(function (c) {
        return c != "";
      }).map(function (c) {
        classes[c] = true;
      });
    } else if ('object' === _typeof(arg)) {
      for (var key in arg) {
        classes[key] = arg[key];
      }
    }
  }

  return Object.keys(classes).map(function (k) {
    return classes[k] && k || '';
  }).join(' ');
};

var Page = createReactClass({
  displayName: "Page",
  render: function render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_body"
    }, /*#__PURE__*/React.createElement("h1", {
      className: "title_text"
    }, "Orders"), /*#__PURE__*/React.createElement("div", {
      className: "title_login_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_username"
    }, "John Doe"), /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "title_btn w-button"
    }, "Login")))), /*#__PURE__*/React.createElement("div", {
      className: "search_form_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "form-block w-form"
    }, /*#__PURE__*/React.createElement("form", {
      id: "email-form",
      name: "email-form",
      "data-name": "Email Form",
      method: "get",
      className: "search_form_body"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "name-4",
      className: "search_form_text"
    }, "Search"), /*#__PURE__*/React.createElement("div", {
      className: "search_form_icon"
    }, "\uF002"), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "text-field search_input w-input",
      maxLength: 256,
      name: "name-3",
      "data-name": "Name 3",
      placeholder: "",
      id: "name-3"
    }), /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "search_btn w-button"
    }, "Submit")), /*#__PURE__*/React.createElement("div", {
      className: "w-form-done"
    }, /*#__PURE__*/React.createElement("div", null, "Thank you! Your submission has been received!")), /*#__PURE__*/React.createElement("div", {
      className: "w-form-fail"
    }, /*#__PURE__*/React.createElement("div", null, "Oops! Something went wrong while submitting the form.")))), /*#__PURE__*/React.createElement("div", {
      className: "table"
    }, /*#__PURE__*/React.createElement("div", {
      className: "table2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "table_container_header"
    }, /*#__PURE__*/React.createElement("div", {
      className: "table_container_row_header"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f33-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f34-2c93edb3",
      className: "text_header_column"
    }, "Command Number")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f36-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f37-2c93edb3",
      className: "text_header_column"
    }, "Customer")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f39-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f3a-2c93edb3",
      className: "text_header_column"
    }, "Address")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f3c-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Quantity")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f3f-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Details")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f42-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Pay")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_6eae559b-c311-287e-3bac-853f4cbd5f99-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Delete")))), /*#__PURE__*/React.createElement("div", {
      className: "table_container_body"
    }, orders.map(function (order) {
      return /*#__PURE__*/React.createElement("div", {
        className: "table_container_row"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408ceba-4f6d1c5f",
        className: "container_text_cn"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cebb-4f6d1c5f",
        className: "text_cn"
      }, order.remoteid)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cebd-4f6d1c5f",
        className: "container_text_customer"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cebe-4f6d1c5f",
        className: "text_customer"
      }, order.custom.customer.full_name)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cec0-4f6d1c5f",
        className: "container_text_address"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cec1-4f6d1c5f",
        className: "text_address"
      }, order.custom.billing_address)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cec5-4f6d1c5f",
        className: "container_text_quantity"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_quantity"
      }, order.items)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cec8-4f6d1c5f",
        className: "container_text_details"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_icon"
      }, "\uF061")), /*#__PURE__*/React.createElement("div", {
        id: "w-node-ce5f7a63-e01a-d15e-4ce6-3f760408cecb-4f6d1c5f",
        className: "container_text_pay"
      }, /*#__PURE__*/React.createElement("div", {
        className: "icon_pay"
      }, "\uF061"), /*#__PURE__*/React.createElement("div", {
        className: "text_pay"
      }, "Status Init", /*#__PURE__*/React.createElement("br", null), "Payment methode : Delivery")));
    })))), /*#__PURE__*/React.createElement("div", {
      className: "navigation"
    }, /*#__PURE__*/React.createElement("div", {
      className: "nav_item"
    }, /*#__PURE__*/React.createElement("div", {
      className: "link link_nav_page"
    }, "1"))));
  }
});
var Child = createReactClass({
  displayName: "Child",
  render: function render() {
    var _this$props$handlerPa = _toArray(this.props.handlerPath),
        ChildHandler = _this$props$handlerPa[0],
        rest = _this$props$handlerPa.slice(1);

    return /*#__PURE__*/React.createElement(ChildHandler, _extends({}, this.props, {
      handlerPath: rest
    }));
  }
});
var Layout = createReactClass({
  displayName: "Layout",
  getInitialState: function getInitialState() {
    return {
      modal: null,
      loader: false
    };
  },
  modal: function modal(spec) {
    var _this2 = this;

    this.setState({
      modal: _objectSpread(_objectSpread({}, spec), {}, {
        callback: function callback(res) {
          _this2.setState({
            modal: null
          }, function () {
            if (spec.callback) spec.callback(res);
          });
        }
      })
    });
  },
  loader: function loader(promiseCallback) {
    var _this3 = this;

    return new Promise(function (resolve, reject) {
      _this3.setState({
        loader: true
      });

      promiseCallback.then(function (res) {
        _this3.setState({
          loader: false
        });

        resolve(res);
      });
    });
  },
  render: function render() {
    var modal_component = {
      'delete': function _delete(props) {
        return /*#__PURE__*/React.createElement(DeleteModal, props);
      }
    }[this.state.modal && this.state.modal.type];
    modal_component = modal_component && modal_component(this.state.modal);
    console.log({
      "Layout state": this
    });

    var props = _objectSpread(_objectSpread({}, this.props), {}, {
      modal: this.modal,
      loader: this.loader
    });

    return /*#__PURE__*/React.createElement("div", {
      className: "layout"
    }, /*#__PURE__*/React.createElement("div", {
      className: cn("modal-wrapper hidden", {
        'hidden': !modal_component
      })
    }, modal_component), /*#__PURE__*/React.createElement("div", {
      className: cn("loader-wrapper hidden", {
        'hidden': !this.state.loader
      })
    }, /*#__PURE__*/React.createElement(Loader, null)), /*#__PURE__*/React.createElement("div", {
      className: "layout-container"
    }, /*#__PURE__*/React.createElement(this.props.Child, props)));
  }
});
var Header = createReactClass({
  displayName: "Header",
  render: function render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "header"
    }, /*#__PURE__*/React.createElement("div", {
      className: "header-container"
    }, /*#__PURE__*/React.createElement(this.props.Child, this.props)));
  }
});
var Orders = createReactClass({
  displayName: "Orders",
  statics: {
    remoteProps: [remoteProps.orders]
  },
  getInitialState: function getInitialState() {
    var _this4 = this;

    return {
      modalArg: {
        type: 'delete',
        title: 'Order deletion',
        message: "Are you sure you want to delete this ?",
        callback: function callback(value) {
          if (value === true) {
            _this4.props.loader(HTTP.delete("/api/order/".concat(_this4.state.modalArg.commandNumber))).then(function (res) {
              browserState = {
                Child: browserState.Child,
                cookie: browserState.cookie,
                handlerPath: browserState.handlerPath,
                path: browserState.path,
                qs: browserState.qs,
                route: browserState.route
              };
              GoTo("orders", null, null);
            });
          }

          return value;
        }
      },
      searchInput: ""
    };
  },
  handleInputChange: function handleInputChange(e) {
    var myValue = e.target.value;
    this.setState({
      searchInput: myValue
    });
  },
  render: function render() {
    var _this5 = this;

    console.log({
      "Props from Layout": this.props
    });
    var currentPage = this.props.qs.page ? parseInt(this.props.qs.page) : 0;
    console.log({
      "Current Page": currentPage
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_body"
    }, /*#__PURE__*/React.createElement("h1", {
      className: "title_text"
    }, "Orders"), /*#__PURE__*/React.createElement("div", {
      className: "title_login_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_username"
    }, "John Doe"), /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "title_btn w-button"
    }, "Login")))), /*#__PURE__*/React.createElement("div", {
      className: "search_form_container"
    }, /*#__PURE__*/React.createElement("form", {
      id: "email-form",
      name: "email-form",
      "data-name": "Email Form",
      method: "get",
      className: "search_form_body"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "name-4",
      className: "search_form_text"
    }, "Search"), /*#__PURE__*/React.createElement("div", {
      className: "search_form_icon"
    }, "\uF002"), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "text-field search_input w-input",
      maxLength: 256,
      name: "name-3",
      "data-name": "Name 3",
      placeholder: "",
      id: "name-3",
      value: this.state.searchInput,
      onInput: function onInput(e) {
        return _this5.handleInputChange(e);
      }
    }), /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "search_btn w-button",
      onClick: function onClick() {
        payload = _this5.state.searchInput.split(":");
        if (payload.length === 2) GoTo("orders", null, _defineProperty({}, payload[0], payload[1]));
      }
    }, "Submit"))), /*#__PURE__*/React.createElement("div", {
      className: "table"
    }, /*#__PURE__*/React.createElement("div", {
      className: "table2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "table_container_header"
    }, /*#__PURE__*/React.createElement("div", {
      className: "table_container_row_header"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f33-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f34-2c93edb3",
      className: "text_header_column"
    }, "Command Number")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f36-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f37-2c93edb3",
      className: "text_header_column"
    }, "Customer")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f39-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f3a-2c93edb3",
      className: "text_header_column"
    }, "Address")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f3c-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Quantity")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f3f-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Details")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f42-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Pay")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_6eae559b-c311-287e-3bac-853f4cbd5f99-2c93edb3",
      className: "table_header_column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text_header_column"
    }, "Delete")))), /*#__PURE__*/React.createElement("div", {
      className: "table_container_body"
    }, this.props.orders.value.map(function (order) {
      return /*#__PURE__*/React.createElement("div", {
        className: "table_container_row"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f47-2c93edb3",
        className: "container_text_cn"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f48-2c93edb3",
        className: "text_cn"
      }, order.id)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f4a-2c93edb3",
        className: "container_text_customer"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f4b-2c93edb3",
        className: "text_customer"
      }, order.full_name)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f4d-2c93edb3",
        className: "container_text_address"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f4e-2c93edb3",
        className: "text_address"
      }, order.email)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f52-2c93edb3",
        className: "container_text_quantity"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_quantity"
      }, order.quantity)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f55-2c93edb3",
        className: "container_text_details"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_icon",
        onClick: function onClick() {
          return GoTo("order", order.id, null);
        }
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_icon"
      }, "\uF061"))), /*#__PURE__*/React.createElement("div", {
        id: "w-node-b517adbb-03af-49b6-03ef-06eacee85f58-2c93edb3",
        className: "container_text_pay"
      }, /*#__PURE__*/React.createElement("div", {
        className: "icon_pay"
      }, "\uF061"), /*#__PURE__*/React.createElement("div", {
        className: "text_pay"
      }, "Status : ", order.status, /*#__PURE__*/React.createElement("br", null), "Payment Method : ", order.shipping_method)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-_3befc68a-4cc7-f89f-4b42-2d7873c83b9e-2c93edb3",
        className: "container_icon_delete"
      }, /*#__PURE__*/React.createElement("div", {
        className: "icon_delete",
        onClick: function onClick() {
          _this5.state.modalArg.commandNumber = order.id;

          _this5.props.modal(_this5.state.modalArg);
        }
      }, /*#__PURE__*/React.createElement("div", {
        className: "icon_delete"
      }, "\uF2ED"))));
    })))), /*#__PURE__*/React.createElement("div", {
      className: "navigation"
    }, currentPage !== 0 ? /*#__PURE__*/React.createElement("div", {
      className: "nav_item"
    }, /*#__PURE__*/React.createElement("div", {
      className: "link link_nav_page",
      onClick: function onClick() {
        return GoTo("orders", null, {
          "page": currentPage - 1
        });
      }
    }, currentPage - 1)) : null, /*#__PURE__*/React.createElement("div", {
      className: "nav_item"
    }, /*#__PURE__*/React.createElement("div", {
      className: "link link_nav_page",
      onClick: function onClick() {
        return GoTo("orders", null, {
          "page": currentPage
        });
      }
    }, currentPage)), /*#__PURE__*/React.createElement("div", {
      className: "nav_item"
    }, /*#__PURE__*/React.createElement("div", {
      className: "link link_nav_page",
      onClick: function onClick() {
        return GoTo("orders", null, {
          "page": currentPage + 1
        });
      }
    }, currentPage + 1))));
  }
});
var Order = createReactClass({
  displayName: "Order",
  statics: {
    remoteProps: [remoteProps.order]
  },
  render: function render() {
    var order = this.props.order.value;
    var shipping_address = order.shipping_address;
    var items = order.items;
    return /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_container"
    }, /*#__PURE__*/React.createElement("h1", {
      className: "title_text"
    }, "Orders"), /*#__PURE__*/React.createElement("div", {
      className: "title_login_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title_username"
    }, "John Doe"), /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "w-button"
    }, "Login"))), /*#__PURE__*/React.createElement("div", {
      className: "order_containter"
    }, /*#__PURE__*/React.createElement("div", {
      className: "customer_container"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-b67fc24a-d7db-950d-b2ca-a4bc9068c3fe-014faf99",
      className: "customer_container_name_title"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab89d-014faf99",
      className: "customer_name_title"
    }, "Client :")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_310807b1-ba6d-605f-b0b8-77db57c7b80a-014faf99",
      className: "customer_container_name"
    }, /*#__PURE__*/React.createElement("div", {
      className: "customer_name"
    }, order.customer_name)), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_8aa61b67-7b61-5628-ff33-589d74396a47-014faf99",
      className: "customer_container_address_title"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab8a1-014faf99",
      className: "customer_address_title"
    }, "Address :")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_30bbb6a1-e938-05c6-d1ec-d4492d9c812c-014faf99",
      className: "customer_container_address"
    }, /*#__PURE__*/React.createElement("div", {
      className: "customer_address"
    }, "".concat(shipping_address.street.join(" "), " ").concat(shipping_address.city, " ").concat(shipping_address.postcode))), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_7837fcd6-5046-8c62-c379-e1b52a05bac5-014faf99",
      className: "customer_container_cn_title"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab8a5-014faf99",
      className: "customer_cn_title"
    }, "Command Number :")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-cea3bd75-fbfa-f3b1-014e-baabb67387aa-014faf99",
      className: "customer_container_cn"
    }, /*#__PURE__*/React.createElement("div", {
      className: "customer_cn"
    }, order.id))), /*#__PURE__*/React.createElement("div", {
      className: "customer_product_container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "order_table_columns_title"
    }, /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab8ab-014faf99",
      className: "column_title"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-block-5"
    }, "Product Name")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab8ae-014faf99",
      className: "column_title"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-block-5"
    }, "Quantity")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab8b1-014faf99",
      className: "column_title"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-block-5"
    }, "Unit price")), /*#__PURE__*/React.createElement("div", {
      id: "w-node-c40df487-7ec0-0bd6-763e-3545c04ab8b4-014faf99",
      className: "column_title"
    }, /*#__PURE__*/React.createElement("div", {
      className: "text-block-5 end-row-right"
    }, "Total Price"))), /*#__PURE__*/React.createElement("div", {
      id: "w-node-_4753eed3-1119-5c1e-eec3-71642a316539-014faf99",
      className: "order_table_body"
    }, items.map(function (item) {
      return /*#__PURE__*/React.createElement("div", {
        className: "order_table_row"
      }, /*#__PURE__*/React.createElement("div", {
        id: "w-node-_0fc04a2c-564d-da61-246b-e127ff1c939e-014faf99",
        className: "order_product_name"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_product_name"
      }, item.product_title)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-_0fc04a2c-564d-da61-246b-e127ff1c93a1-014faf99",
        className: "order_quantity"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_quantity"
      }, item.quantity_to_fetch)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-_0fc04a2c-564d-da61-246b-e127ff1c93a4-014faf99",
        className: "order_unit_price"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_unit_price"
      }, item.unit_price)), /*#__PURE__*/React.createElement("div", {
        id: "w-node-_0fc04a2c-564d-da61-246b-e127ff1c93a7-014faf99",
        className: "order_total_price"
      }, /*#__PURE__*/React.createElement("div", {
        className: "text_total_price end-row-right"
      }, item.quantity_to_fetch * item.unit_price)));
    }))), /*#__PURE__*/React.createElement("div", {
      className: "previous_container"
    }, /*#__PURE__*/React.createElement("a", {
      href: "/"
    }, "Go Back"))));
  }
});
var ErrorPage = createReactClass({
  displayName: "ErrorPage",
  render: function render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "loader-wrapper"
    }, /*#__PURE__*/React.createElement("div", {
      className: "loader-content"
    }, /*#__PURE__*/React.createElement("img", {
      src: "https://uploads-ssl.webflow.com/628360c0753f8a635a6d1c5e/6284e1b54c694dc848dc5147_test.svg",
      loading: "lazy",
      alt: ""
    })));
  }
});
var DeleteModal = createReactClass({
  displayName: "DeleteModal",
  render: function render() {
    var _this6 = this;

    return /*#__PURE__*/React.createElement("div", {
      className: "modal-content"
    }, /*#__PURE__*/React.createElement("div", {
      className: "modal_container_header"
    }, /*#__PURE__*/React.createElement("h1", {
      className: "modal_text_header"
    }, this.props.message)), /*#__PURE__*/React.createElement("div", {
      className: "modal_container_body"
    }, /*#__PURE__*/React.createElement("div", {
      className: "modal_container_btn"
    }, /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "modal_btn_yes modal_btn w-button",
      onClick: function onClick() {
        return _this6.props.callback(true);
      }
    }, /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "modal_btn_yes modal_btn w-button"
    }, "Oui"))), /*#__PURE__*/React.createElement("div", {
      className: "modal_container_btn"
    }, /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "modal_btn_no modal_btn w-button",
      onClick: function onClick() {
        return _this6.props.callback(false);
      }
    }, /*#__PURE__*/React.createElement("a", {
      href: "#",
      className: "modal_btn_no modal_btn w-button"
    }, "Non")))));
  }
});
var Loader = createReactClass({
  displayName: "Loader",
  render: function render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "loader-wrapper"
    }, /*#__PURE__*/React.createElement("div", {
      className: "loader-content"
    }, /*#__PURE__*/React.createElement("img", {
      src: "https://uploads-ssl.webflow.com/628360c0753f8a635a6d1c5e/6284e1b54c694dc848dc5147_test.svg",
      loading: "lazy",
      alt: ""
    })));
  }
});
var routes = {
  "orders": {
    path: function path(params) {
      return "/";
    },
    match: function match(path, qs) {
      // return (path == "/") && {handlerPath: [Page]} // Note that we use the "&&" expression to simulate a IF statement
      return path == "/" && {
        handlerPath: [Layout, Header, Orders]
      };
    }
  },
  "order": {
    path: function path(params) {
      return "/order/" + params;
    },
    match: function match(path, qs) {
      var r = new RegExp("/order/([^/]*)$").exec(path);
      return r && {
        handlerPath: [Layout, Header, Order],
        order_id: r[1]
      }; // Note that we use the "&&" expression to simulate a IF statement
    }
  }
};

var GoTo = function GoTo(route, params, query) {
  console.log({
    "Route": route
  });
  console.log({
    "Params": params
  });
  console.log({
    "Query": query
  });
  var qs = Qs.stringify(query); // console.log({"routes": routes})
  // console.log({"route": route})

  var url = routes[route].path(params) + (qs == '' ? '' : '?' + qs);
  console.log({
    "Url": url
  });
  history.pushState({}, "", url);
  onPathChange();
};

var browserState = {
  Child: Child
};

function onPathChange() {
  var path = location.pathname;
  var qs = Qs.parse(location.search.slice(1));
  var cookies = Cookie.parse(document.cookie);
  browserState = _objectSpread(_objectSpread({}, browserState), {}, {
    path: path,
    qs: qs,
    cookie: cookies
  }); // @todo remove log
  // console.log({"browserState": browserState})
  // console.log({"routes": routes})

  var route; // We try to match the requested path to one our our routes

  for (var key in routes) {
    routeProps = routes[key].match(path, qs);

    if (routeProps) {
      route = key;
      break;
    }
  } // GOAL => check the list of components in the HandlerPath and get the static remoteProps list for each of them
  // Call the corresponding URLs on our API, and retrieve the remote data
  // addRemoteProps(browserState)
  // We add the route name and the route Props to the global browserState


  browserState = _objectSpread(_objectSpread(_objectSpread({}, browserState), routeProps), {}, {
    route: route
  }); // // If the path in the URL doesn't match with any of our routes, we render an Error component (we will have to create it later)
  // if (!route)
  //   return ReactDOM.render(<ErrorPage message={"Not Found"} code={404}/>, document.getElementById('root'))
  // // If we found a match, we render the Child component, which will render the handlerPath components recursively, remember ? ;)
  // ReactDOM.render(<Child {...browserState}/>, document.getElementById('root'));

  addRemoteProps(browserState).then(function (props) {
    browserState = props; // Log our new browserState
    // console.log({"Is this my browser State ??": browserState})
    // Render our components using our remote data

    ReactDOM.render( /*#__PURE__*/React.createElement(Child, browserState), document.getElementById('root'));
  }, function (res) {
    ReactDOM.render( /*#__PURE__*/React.createElement(ErrorPage, {
      message: "Shit happened",
      code: res.http_code
    }), document.getElementById('root'));
  });
}

window.addEventListener("popstate", function () {
  onPathChange();
});
onPathChange(); // We also call onPathChange once when the js is loaded

/***/ }),

/***/ "./node_modules/file-loader/dist/cjs.js?name=[name].[ext]!./index.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/file-loader/dist/cjs.js?name=[name].[ext]!./index.html ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__webpack_require__.p + "index.html");

/***/ })

};
;
//# sourceMappingURL=server.components_app_js.js.map