defmodule HelloPort do
  use GenServer

  # Process Parent
  def start_link() do
    GenServer.start_link(HelloPort, {"node hello.js", 0, cd: "node_server/"}, name: Hello)
  end

  def call(:hello) do
    GenServer.call(Hello, :hello)
  end

  def call(:what) do
    GenServer.call(Hello, :what)
  end

  def cast({:kbrw, value}) do
    GenServer.cast(Hello, {:kbrw, value})
  end

  def call(:kbrw) do
    GenServer.call(Hello, :kbrw)
  end

  # Process Child
  @impl true
  def init({cmd, init, opts}) do
    IO.inspect cmd
    port = Port.open({:spawn, '#{cmd}'}, [:binary, :exit_status, packet: 4] ++ opts)
    send(port,{self(),{:command,:erlang.term_to_binary(init)}})
    {:ok,port}
  end

  @impl true
  def handle_call(:hello, _, port) do
    send(port, {self(), {:command, :erlang.term_to_binary(:hello)}})
    res = receive do {^port,{:data,b}}->:erlang.binary_to_term(b) end
    {:reply, res, port}
  end

  @impl true
  def handle_call(:what, _, port) do
    send(port, {self(), {:command, :erlang.term_to_binary(:what)}})
    res = receive do {^port,{:data,b}}->:erlang.binary_to_term(b) end
    {:reply, res, port}
  end

  @impl true
  def handle_call(:kbrw, _, port) do
    send(port, {self(), {:command, :erlang.term_to_binary(:get)}})
    res = receive do {^port,{:data,b}}->:erlang.binary_to_term(b) end
    {:reply, res, port}
  end

  @impl true
  def handle_cast({:kbrw, value}, port) do
    send(port,{self(),{:command,:erlang.term_to_binary({:add, value})}})
    {:noreply,port}
  end

end
