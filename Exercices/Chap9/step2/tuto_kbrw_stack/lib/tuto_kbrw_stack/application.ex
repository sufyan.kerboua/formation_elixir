defmodule TutoKbrwStack.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do

    Application.put_env(
      :reaxt,:global_config,
      Map.merge(
        Application.get_env(:reaxt,:global_config), %{localhost: "http://localhost:4000"}
      )
    )
    Reaxt.reload

    children = [
      {OrderFSMSupervisor, []},
      Server.Database,
      # {Plug.Cowboy, scheme: :http, plug: Server.Router, options: [port: 4001] },
      {Plug.Cowboy, scheme: :http, plug: Server.EwebRouter, options: [port: 4000] }
    ]
    opts = [strategy: :one_for_one, name: TutoKbrwStack.Supervisor]
    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    Supervisor.start_link(children, opts)
  end
end
