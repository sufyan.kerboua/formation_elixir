defmodule ErrorRoutes do
  use Ewebmachine.Builder.Resources ; resources_plugs

  resource "/error/:status" do %{s: elem(Integer.parse(status),0)} after
    content_types_provided do: ['text/html': :to_html, 'application/json': :to_json]

    defh to_html, do: "<h1> Error ! : '#{Ewebmachine.Core.Utils.http_label(state.s)}'</h1>"
    defh to_json, do: ~s/{"error": #{state.s}, "label": "#{Ewebmachine.Core.Utils.http_label(state.s)}"}/

    finish_request do: {:halt,state.s}
  end
end

defmodule MyJSONApi do
  use Ewebmachine.Builder.Handlers
  plug :cors
  plug :add_handlers

  content_types_provided do: ["application/json": :to_json]
  defh to_json, do: Poison.encode!(state[:json_obj])

  defp cors(conn,_), do:
    put_resp_header(conn,"Access-Control-Allow-Origin","*")
end

defmodule Server.EwebRouter do
  use Ewebmachine.Builder.Resources

  # Debug Mode
  if Mix.env == :dev, do: plug Ewebmachine.Plug.Debug
  resources_plugs error_forwarding: "/error/:status", nomatch_404: true
  plug ErrorRoutes

  plug :resource_match
  plug Ewebmachine.Plug.Run
  plug Ewebmachine.Plug.Send

  resource "/hello/:name" do %{name: name} after
    content_types_provided do: ['text/html': :to_html]
    defh to_html, do: "<html><h1>Hello #{state.name}</h1></html>"
  end

  resource "/api/orders/" do %{} after
    plug MyJSONApi
    allowed_methods do: ["GET"]
    resource_exists do: pass(true, json_obj: Riak.get_orders(conn))
  end

  resource "/api/order/:order_id" do %{order_id: order_id} after
    plug MyJSONApi
    allowed_methods(do: ["GET","DELETE"])
    resource_exists do: pass(true, json_obj: Riak.get_order(state.order_id))
    delete_resource do
      pass(true, json_obj: Riak.delete_order(state.order_id))
      :timer.sleep 2000
    end
  end


  resource "/api/pay/:order_id" do %{order_id: order_id} after
    plug MyJSONApi
    allowed_methods(do: ["GET"])
    resource_exists do
      OrderFSMSupervisor.startOrderFSM(state.order_id)
      order = OrderFSM.update_order(state.order_id)
      res = case order do
        :action_unavailable ->
          %{"error" => "Action pay unavalable for this order"}
        _ ->
          order
      end
      Process.sleep(2000)
      pass(true, json_obj: res)
    end
  end

  resource "/*path" do %{path: Enum.join(path, "/")} after
    require EEx
    plug Plug.Static, at: "/public", from: :tuto_kbrw_stack
    EEx.function_from_file(:defp, :layout, "web/layout.html.eex", [:render])
    content_types_provided do: ["text/html": :to_html]
    defh to_html(conn, state) do
      Reaxt.render!(:app, %{path: conn.request_path, cookies: conn.cookies, query: conn.params},30_000)
      |> layout
    end
  end

  # resource "/" do %{} after
  #   allowed_methods(do: ["GET"])
  #   content_types_provided do: ["text/html;charset=utf-8": :to_html]
  #   defh to_html, do: Poison.encode!(state[:json_obj])
  #   # resource_exists(do: pass(true, json_obj: %{Name: "Nil"}))
  #   resource_exists do
  #     EEx.function_from_file :defp, :layout, "web/layout.html.eex", [:render]
  #     render = Reaxt.render!(:app, %{path: conn.request_path, cookies: conn.cookies, query: conn.params},30_000)
  #     layout(render)
  #   end
  # end

  # get "/api/orders/" do
  #   queries = String.split(conn.query_string, "&") |> Enum.map(fn query -> String.split(query, "=") end)
  #   page = queries |> Enum.filter(fn payload -> List.first(payload) == "page" end) |> List.first
  #   rows = queries |> Enum.filter(fn payload -> List.first(payload) == "rows" end) |> List.first
  #   sort = queries |> Enum.filter(fn payload -> List.first(payload) == "sort" end) |> List.first
  #   query = queries |> Enum.filter(fn payload -> List.first(payload) !== "page" and List.first(payload) !== "rows" end) |> Enum.map(fn payload -> Enum.join(payload, ":") end) |>  Enum.join("%20AND%20")
  #   # raw_bdd = Server.Database.get(Server.Database)
  #   raw_bdd = Server.Database.search(Server.Database, {query, page, rows, sort})
  #   formated_res = Poison.encode!(raw_bdd)
  #   send_resp(conn, 200, formated_res)
  # end

end
