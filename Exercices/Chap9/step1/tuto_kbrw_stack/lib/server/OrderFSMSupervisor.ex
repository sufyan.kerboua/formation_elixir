defmodule OrderFSMSupervisor do
  # Automatically defines child_spec/1
  use DynamicSupervisor

  def start_link(init_arg) do
    IO.inspect "init_arg"
    IO.inspect init_arg
    DynamicSupervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  # @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one, restart: :transient)
  end

  def startOrderFSM(id) do
    res = case DynamicSupervisor.start_child(__MODULE__, {OrderFSM, id}) do
      {:ok, pid} ->
        pid
      {:error, {:already_started, pid}} ->
        pid
      error -> raise error
    end
    res
  end
end
