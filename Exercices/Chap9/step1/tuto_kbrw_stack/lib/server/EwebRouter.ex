defmodule Server.EwebRouter do
  use Ewebmachine.Builder.Resources
  # Debug Mode
  if Mix.env == :dev, do: plug Ewebmachine.Plug.Debug
  plug :resource_match
  plug Ewebmachine.Plug.Run
  plug Ewebmachine.Plug.Send

  resource "/hello/:name" do %{name: name} after
    allowed_methods do: ["GET"]
    content_types_provided do: ['application/json': :to_json]
    defh to_json, do: Poison.encode!(state.name)
  end

end
