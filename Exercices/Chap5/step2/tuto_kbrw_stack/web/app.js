require('!!file-loader?name=[name].[ext]!./index.html')
/* required library for our React app */
var ReactDOM = require('react-dom')
var React = require("react")
var createReactClass = require('create-react-class');
const { render } = require('@testing-library/react');
var Qs = require('qs')
var Cookie = require('cookie')

/* required css for our application */
require('./webflow/home/home.css');

require('./webflow/order/order.css');

require('./webflow/orders/orders.css');

require('./webflow/loader/loader.css');

var orders = [
  {remoteid: "000000189", custom: {customer: {full_name: "TOTO & CIE"}, billing_address: "Some where in the world"}, items: 2}, 
  {remoteid: "000000190", custom: {customer: {full_name: "Looney Toons"}, billing_address: "The Warner Bros Company"}, items: 3}, 
  {remoteid: "000000191", custom: {customer: {full_name: "Asterix & Obelix"}, billing_address: "Armorique"}, items: 29}, 
  {remoteid: "000000192", custom: {customer: {full_name: "Lucky Luke"}, billing_address: "A Cowboy doesn't have an address. Sorry"}, items: 0}, 
]

var Page = createReactClass({
  render() {
    return (
      <JSXZ in="orders/orders" sel=".container">
        <Z sel=".table_container_body">
          {
            orders.map(order => (
              <JSXZ in="home/home" sel=".table_container_row">
                <Z sel=".text_cn">{order.remoteid}</Z>
                <Z sel=".text_customer">{order.custom.customer.full_name}</Z>
                <Z sel=".text_address">{order.custom.billing_address}</Z>
                <Z sel=".text_quantity">{order.items}</Z>
              </JSXZ>
            ))
          }
        </Z>
      </JSXZ>
    )
  }
})

var Child = createReactClass({
  render(){
    var [ChildHandler, ...rest] = this.props.handlerPath
    return <ChildHandler {...this.props} handlerPath={rest} />
  }
})

var Layout = createReactClass({
  render(){
    return <JSXZ in="orders/orders" sel=".layout">
        <Z sel=".layout-container">
          <this.props.Child {...this.props}/>
        </Z>
      </JSXZ>
    }
})

var Header = createReactClass({
  render(){
    return <JSXZ in="orders/orders" sel=".header">
        <Z sel=".header-container">
          <this.props.Child {...this.props}/>
        </Z>
      </JSXZ>
    }
})

var Orders = createReactClass({
  render(){
    return (
      <JSXZ in="orders/orders" sel=".container">
        <Z sel=".table_container_body">
          {
            orders.map(order => (
              <JSXZ in="home/home" sel=".table_container_row">
                <Z sel=".text_cn">{order.remoteid}</Z>
                <Z sel=".text_customer">{order.custom.customer.full_name}</Z>
                <Z sel=".text_address">{order.custom.billing_address}</Z>
                <Z sel=".text_quantity">{order.items}</Z>
              </JSXZ>
            ))
          }
        </Z>
      </JSXZ>
    )
    }
})

var Order = createReactClass({
  render(){
    return <JSXZ in="order/order" sel=".container">
      </JSXZ>
    }
})

var ErrorPage = createReactClass({
  render(){
    return <JSXZ in="loader/loader" sel=".loader-wrapper">
      </JSXZ>
    }
})

var routes = {
  "orders": {
    path: (params) => {
      return "/";
    },
    match: (path, qs) => {
      // return (path == "/") && {handlerPath: [Page]} // Note that we use the "&&" expression to simulate a IF statement
      return (path == "/") && {handlerPath: [Layout, Header, Orders]}
    }
  }, 
  "order": {
    path: (params) => {
      return "/order/" + params;
    },
    match: (path, qs) => {
      var r = new RegExp("/order/([^/]*)$").exec(path)
      return r && {handlerPath: [Layout, Header, Order],  order_id: r[1]} // Note that we use the "&&" expression to simulate a IF statement
    }
  }
}

var browserState = {Child: Child}

function onPathChange() {
  var path = location.pathname
  var qs = Qs.parse(location.search.slice(1))
  var cookies = Cookie.parse(document.cookie)

  browserState = {
    ...browserState, 
    path: path, 
    qs: qs, 
    cookie: cookies
  }
  var route

  // @todo remove
  console.log("Hello There")
  console.log(routes)

  // We try to match the requested path to one our our routes
  for (var key in routes) {
    routeProps = routes[key].match(path, qs)
    if (routeProps){
        route = key
          break;
    }
  }
  // We add the route name and the route Props to the global browserState
  browserState = {
    ...browserState,
    ...routeProps,
    route: route
  }

  // If the path in the URL doesn't match with any of our routes, we render an Error component (we will have to create it later)
  if (!route)
    return ReactDOM.render(<ErrorPage message={"Not Found"} code={404}/>, document.getElementById('root'))

  // If we found a match, we render the Child component, which will render the handlerPath components recursively, remember ? ;)
  ReactDOM.render(<Child {...browserState}/>, document.getElementById('root'));
}

window.addEventListener("popstate", ()=>{ onPathChange() })
onPathChange() // We also call onPathChange once when the js is loaded

// ReactDOM.render(<Page />, document.getElementById('root'));
